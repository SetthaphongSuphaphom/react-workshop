import * as React from "react";
import CssBaseline from "@mui/material/CssBaseline";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import { Typography } from "@mui/material";
import Button from "@mui/material/Button";
import Paper from "@mui/material/Paper";
import { useState, useEffect } from "react";
import TableContainer from "@mui/material/TableContainer";
import Avatar from "@mui/material/Avatar";
import Link from "@mui/material/Link";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";

function createData(name, calories, fat, carbs, protein) {
  return { name, calories, fat, carbs, protein };
}

const rows = [
  createData("Frozen yoghurt", 159, 6.0, 24, 4.0),
  createData("Ice cream sandwich", 237, 9.0, 37, 4.3),
  createData("Eclair", 262, 16.0, 24, 6.0),
  createData("Cupcake", 305, 3.7, 67, 4.3),
  createData("Gingerbread", 356, 16.0, 49, 3.9),
];

export default function MediaCard() {
  const [items, setItems] = useState([]);

  useEffect(() => {
    UserGet();
  }, []);

  const UserGet = () => {
    fetch("https://www.melivecode.com/api/users")
      .then((res) => res.json())
      .then((result) => {
        setItems(result);
      });
  };

  const UserUpdate = (id) => {
    window.location = "/update/" + id;
  };

  const UserDelete = (id) => {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    var raw = JSON.stringify({
      id: id,
    });

    var requestOptions = {
      method: "DELETE",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };

    fetch("https://www.melivecode.com/api/users/delete", requestOptions)
      .then((response) => response.json())
      .then((result) => {
        alert(result["message"]);
        if (result["status"] === "ok") {
          UserGet();
        }
      })
      .catch((error) => console.log("error", error));
  };

  return (
    <>
      <React.Fragment>
        <CssBaseline />
        <Container maxWidth="lg" sx={{ p: 2}}>
          <Paper sx={{ p: 2}} style={{background:"#ffffaa"}}>
            <Box display="flex">
              <Box sx={{ flexGrow: 1 }}>
                <Typography variant="h6" gutterBottom component="div">
                  Users
                </Typography>
              </Box>
              <Box>
                <Link href="create">
                  <Button variant="contained" style={{background:"#DEB887", color: '#492809', marginBottom:"20px"}}>Create</Button>
                </Link>
              </Box>
            </Box>
            <TableContainer component={Paper} style={{background:"#ffffaa"}}>
            <Box
              sx={{
                display: "grid",
                gridTemplateColumns: "repeat(auto-fill, minmax(320px, 1fr))",
                gap: "70px",
              }}
            >
                {items.map((row) => (
                <Card sx={{ maxWidth: 330 }}>
                  <CardMedia sx={{ height: 200 }}>
                    <Avatar
                      sx={{ height: 200, width: 200, marginLeft:"60px",marginTop:"10px", display:"block" }}
                      alt={row.username}
                      src={row.avatar}
                    />
                  </CardMedia>
                  <CardContent key={row.id}>
                    <Typography gutterBottom variant="h5" component="div">
                      ID:{row.id}
                    </Typography>
                    <Typography variant="body2" color="text.secondary">
                      First Name: {row.fname} <br />
                      Last Name: {row.lname} <br />
                      Username: {row.username} <br />
                    </Typography>
                  </CardContent>
                  <CardActions>
                    <Button style={{color: '#492809', background:"#DEB887"}} onClick={() => UserUpdate(row.id)}>Edit</Button>
                    <Button style={{color: '#492809', background:"#DEB887"}} onClick={() => UserDelete(row.id)}>Del</Button>
                  </CardActions>
                </Card>
              ))}
            </Box>
              
            </TableContainer>
          </Paper>
        </Container>
      </React.Fragment>
    </>
  );
}
