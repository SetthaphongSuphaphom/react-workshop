import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'
import Navber from './Navber.jsx';
import Users from './Users.jsx';
import { Routes,Route,Link ,BrowserRouter} from 'react-router-dom';
import UserCreate from './UserCreate.jsx';
import UserUpdate from './UserUpdate.jsx';

function App() {
  

  return (
    <div>
      
      <Navber/>
        <Routes>
          <Route path='/' element={<Users/>}/>
          <Route path='create' element={<UserCreate/>}/>
          <Route path='update/:id' element={<UserUpdate/>}/>
        </Routes>
    
    </div>
  )
}

export default App
