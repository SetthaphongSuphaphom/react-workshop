import * as React from "react";
import CssBaseline from "@mui/material/CssBaseline";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import Paper from "@mui/material/Paper";
import Link from "@mui/material/Link";
import { useState, useEffect } from "react";
import { ButtonGroup } from "@mui/material";
import CardContent from "@mui/material/CardContent";
import Grid from "@mui/material/Grid";
import "./Card.css";

const urlApiAll =
  "https://735c-110-170-188-98.ngrok-free.app/customer?fbclid=IwAR36jCssLA1whZ5FHRXs0nf1aG3rVsp28aVbW0iLs5NAtL1phfOUTrxxdXY";
const urlApi = "https://735c-110-170-188-98.ngrok-free.app/customer";

export default function SimpleContainer() {
  const [items, setItems] = useState([]);

  useEffect(() => {
    UserGet();
  }, []);

  const UserGet = () => {
    fetch(urlApiAll)
      .then((res) => res.json())
      .then((result) => {
        setItems(result);
      });
  };
  const UserEdit = (id) => {
    window.location = "/edit/" + id;
  };
  const UserDel = (id) => {
    console.log(id);
    console.log(urlApi + "/" + id, requestOptions);
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    var requestOptions = {
      method: "DELETE",
      redirect: "follow",
    };

    fetch(urlApi + "/" + id, requestOptions)
      .then((response) => response.text())
      .then((result) => {
        console.log(result);
        alert(result);
        if (result === "Removed Customer Complete") {
          UserGet();
        }
      })
      .catch((error) => console.log("error", error));
  };

  return (
    <React.Fragment>
      <CssBaseline />
      <Container sx={{ p: 4 }}>
        <Paper sx={{ p: 2 }}>
          <Box display="flex">
            <Box sx={{ flexGrow: 1 }}>
              <Typography variant="h6" gutterBottom>
                Card
              </Typography>
            </Box>
            <Box>
              <Link href="create">
                <Button variant="contained">Create</Button>
              </Link>
            </Box>
          </Box>
          <hr />
          <Box width="90%" margin="50px">
            <Grid
              container
              spacing={1}
              display="flex"
              justifyContent="r"
              minWidth="0px"
            >
              {items.map((row) => (
                <CardContent key={row.Customer_name}>
                  <Typography variant="h6" component="div">
                    Name : {row.Customer_name}
                  </Typography>
                  <Typography
                    sx={{ fontSize: 12 }}
                    color="text.secondary"
                    gutterBottom
                  >
                    ID :{row.Customer_id}
                  </Typography>
                  <Typography
                    sx={{ fontSize: 12 }}
                    color="text.secondary"
                    gutterBottom
                  >
                    Phone Number :{row.Phone_number}
                  </Typography>
                  <Typography sx={{ fontSize: 12 }} color="text.secondary">
                    Birth Date :{row.Birth_date}
                  </Typography>
                  <Typography>
                    <ButtonGroup
                      variant="outlined"
                      aria-label="outlined button group"
                      sx={{ margin: "20px" ,display:"flex",justifyContent:"center"}}
                    >
                      <Button onClick={() => UserEdit(row.id)}>Edit</Button>
                      <Button onClick={() => UserDel(row.id)}>Del</Button>
                    </ButtonGroup>
                  </Typography>
                </CardContent>
              ))}
              {""}
            </Grid>
          </Box>
        </Paper>
      </Container>
    </React.Fragment>
  );
}
