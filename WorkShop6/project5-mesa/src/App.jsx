import React from "react";
import { Routes, Route, Navigate } from "react-router-dom";
import "./App.css";
import Navbar from "./components/Navbar";
import UsersTable from "./components/UsersTable";
import UserCreate from "./components/UserCreate";
import UserUpdate from "./components/UserUpdate";
import UsersCard from "./components/UsersCard";

function App() {
  return (
    <div>
      <Navbar />
      <Routes>
        <Route path="/" element={<UsersTable />} />
        <Route path="/create" element={<UserCreate />} />
        <Route path="/update/:id" element={<UserUpdate />} />
        <Route path="/card" element={<UsersCard />} />
        <Route path="/table" element={<Navigate to="/" />}></Route>
      </Routes>
    </div>
  );
}

export default App;
