import * as React from "react";
import CssBaseline from "@mui/material/CssBaseline";
import Container from "@mui/material/Container";
import { Typography, Grid } from "@mui/material";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import { useState, useEffect } from "react";
import { useParams } from "react-router-dom";

export default function UserUpdate() {
  const { id } = useParams();
  const [name, setName] = useState("");
  const [phone, setPhone] = useState("");

  useEffect(() => {
    var requestOptions = {
      method: "GET",
      redirect: "follow",
    };

    fetch("http://172.16.157.2:5000/customers/" + id, requestOptions)
      .then((response) => response.json())
      .then((result) => {
        // console.log(result)
        setName(result["name"]);
        setPhone(result["phone_number"]);
        console.log(result)
        // if (result["status"] === "Update successfully with ID "+id) {
        //   setName(result["customers"]["name"]);
        //   setPhone(result["customers"]["phone"]);
        //   alert("Successful")
        // }
      })
      .catch((error) => console.log("error", error));
  }, [id]);

  const handleSubmit = (event) => {
    event.preventDefault();
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    var raw = JSON.stringify({
      name: name,
      phone_number: phone,
    });

    var requestOptions = {
      method: "PUT",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };

    fetch("http://172.16.157.2:5000/customers/"+id, requestOptions)
      .then((response) => response.json())
      .then((result) => {
        console.log(result)
        alert(result['message'])
        if (result['message'].includes('Update successfully with ID '+id)){
            window.location.href='/'
        }
      })
      .catch((error) => console.log("error", error));
  };

  return (
    <React.Fragment>
      <CssBaseline />
      <Container maxWidth="sm" sx={{ p: 2 }}>
        <Typography variant="h6" gutterBottom>
          Update User
        </Typography>
        <form onSubmit={handleSubmit}>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField
                id="fname"
                label="Name"
                variant="outlined"
                fullWidth
                required
                onChange={(e) => setName(e.target.value)}
                value={name}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                id="Phone"
                label="Phone"
                variant="outlined"
                fullWidth
                required
                onChange={(e) => setPhone(e.target.value)}
                value={phone}
              />
            </Grid>
            <Grid item xs={12}>
              <Button type="submit" variant="contained" fullWidth>
                Update
              </Button>
            </Grid>
          </Grid>
        </form>
      </Container>
    </React.Fragment>
  );
}
