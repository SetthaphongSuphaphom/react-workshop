import * as React from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import IconButton from "@mui/material/IconButton";
import MenuIcon from "@mui/icons-material/Menu";
import { Link } from "react-router-dom";
import ButtonGroup from "@mui/material/ButtonGroup";

export default function Navbar() {
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>
          <IconButton
            size="large"
            edge="start"
            color="inherit"
            aria-label="menu"
            sx={{ mr: 2 }}
          >
            <MenuIcon />
          </IconButton>

          <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
            CRUD
          </Typography>

          <Box>
            <ButtonGroup
              variant="contained"
              aria-label="contained button group"
            >
              <Link to="/">
                <Button
                  variant="contained"
                  sx={{
                    borderEndEndRadius: 0,
                    borderTopRightRadius: 0,
                  }}
                  aria-label="contained button group"
                >
                  Table
                </Button>
              </Link>

              <Link to="/card">
                <Button
                  variant="contained"
                  sx={{
                    borderTopLeftRadius: 0,
                    borderBottomLeftRadius: 0,
                  }}
                  aria-label="contained button group"
                >
                  Card
                </Button>
              </Link>
            </ButtonGroup>
          </Box>
        </Toolbar>
      </AppBar>
    </Box>
  );
}
