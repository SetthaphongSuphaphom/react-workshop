import { Routes,Route} from "react-router-dom";
import Navbar from "./components/Navbar";
import User from "./components/User";
import UserCreate from "./components/UserCreate";
import UserUpdate from "./components/UserUpdate";
import UsersCard from "./components/UserCard";

function App() {
  return (
    <div >
     <Navbar/>
     <Routes>
       <Route path = "/" element={<User/>}/>
       <Route path = "/create" element= {<UserCreate/>} /> 
       <Route path="/card" element={<UsersCard />} />
       <Route path = "/update/:id" element= {<UserUpdate/>} /> 
     </Routes>
    </div>
  );
}

export default App;


