import Navbar from "./components/Navbar";
import Users from "./components/Users";
import UserCreate from "./components/UserCreate";
import UserUpdate from "./components/UserUpdate";
import "./App.css";
import { Routes, Route, Link } from "react-router-dom";
import Table from "./components/Table";
import MediaCard from "./components/Card"

function App() {
  return (
    <div>
      <Navbar />
      <Routes>
        <Route path="/" element={<Users/>} />
        <Route path="create" element={<UserCreate/>} />
        <Route path="update/:id" element={<UserUpdate/>} />
        <Route path="/card" element={<MediaCard/>} />
        <Route path="/table" element={<Table/>} />
      </Routes>
      
    </div>
  );
}

export default App;
