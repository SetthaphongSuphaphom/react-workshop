import * as React from "react";
import CssBaseline from "@mui/material/CssBaseline";
import Container from "@mui/material/Container";
import { Typography, Grid } from "@mui/material";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import { useState } from "react";

export default function CreateCustomer() {
  const [customer_name, setCustomer_name] = useState("");
  const [phone_number, setPhone_number] = useState("");

  const handleSubmit = (event) => {
    event.preventDefault();
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    var raw = JSON.stringify({
      customer_name: customer_name,
      phone_number: phone_number,
    });

    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };

    fetch("https://4ef6-202-28-119-38.ngrok-free.app/customer", requestOptions)
      .then((response) => response.text())
      .then(() => {
        alert("Create complete");
        window.location.href = "/";
      })
      .catch((error) => console.log("error", error));
  };

  return (
    <React.Fragment>
      <CssBaseline />
      <Container maxWidth="sm" sx={{ p: 2 }}>
        <Typography variant="h6" gutterBottom>
          Create User
        </Typography>
        <form onSubmit={handleSubmit}>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField
                id="customer_name"
                label="Name"
                variant="outlined"
                fullWidth
                required
                onChange={(e) => setCustomer_name(e.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                id="phone_number"
                label="Phone number"
                variant="outlined"
                fullWidth
                required
                onChange={(e) => setPhone_number(e.target.value)}
              />
            </Grid>

            <Grid item xs={12}>
              <Button
                type="submit"
                fullWidth
                color="primary"
                disabled={false}
                size="large"
                variant="filled"
              >
                Create
              </Button>
            </Grid>
          </Grid>
        </form>
      </Container>
    </React.Fragment>
  );
}
