import "./App.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Navbar from "./components/Navbar";
import UserCart from "./components/UserCart";
import UserCreate from "./components/UserCreate";
import UserUpdate from "./components/UserUpdate";
import CardsUser from "./components/CardsUser";

function App() {
  return (
    <BrowserRouter>
      <Navbar />
      <Routes>
        <Route path="/" element={<UserCart />} />
        <Route path="/createUser" element={<UserCreate />} />
        <Route path="/userUpdate/:id" element={<UserUpdate />} />
        <Route path="/cards" element={<CardsUser />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
