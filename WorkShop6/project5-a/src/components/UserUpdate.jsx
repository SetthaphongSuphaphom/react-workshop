import React, { useState, useEffect } from "react";
import CssBaseline from "@mui/material/CssBaseline";
import Container from "@mui/material/Container";
import Typography from "@mui/material/Typography";
import Grid from "@mui/material/Grid";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import { useParams } from "react-router-dom";

export default function UserUpdate() {
  const { id } = useParams();
  const [fname, setFname] = useState("");
  const [lname, setLname] = useState("");
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [avatar, setAvatar] = useState("");
  useEffect(() => {
    console.log("User ID ที่จะมีการ Update คือ : ", id);
    const requestOption = {
      method: "GET",
      redirect: "follow",
    };
    fetch(`https://www.melivecode.com/api/users/${id}`, requestOption)
      .then((res) => res.json())
      .then((result) => {
        if (result.status === "ok") {
          setFname(result.user.fname);
          setLname(result.user.lname);
          setUsername(result.user.username);
          setEmail(result.user.email);
          setAvatar(result.user.avatar);
        }
      })
      .catch((err) => console.log("Error ", err));
  }, [id]);

  const onChangeUser = (event) => {
    event.preventDefault();

    // const myHeaders = new Headers();
    // myHeaders.append("Content-Type", "application/json");
    const myHeader = { "Content-Type": "application/json" };

    const raw = JSON.stringify({
      id: id,
      fname: fname,
      lname: lname,
      username: username,
      email: email,
      avatar: avatar,
    });

    const requestOption = {
      method: "PUT",
      headers: myHeader,
      body: raw,
      redirect: "follow",
    };

    fetch("https://www.melivecode.com/api/users/update", requestOption)
      .then((response) => response.json())
      .then((result) => {
        console.log(result);
        return result;
      })
      .then((result) => {
        if (result.status === "ok") {
          alert(result.message);
          window.location.href = "/";
        }
        return result;
      })
      .catch((err) => console.log("Error ", err));
  };
  return (
    <React.Fragment>
      <CssBaseline />
      <Container maxWidth="sm" sx={{ p: 2 }}>
        <Typography variant="h6" gutterBottom>
          Update User
        </Typography>
        <form onSubmit={onChangeUser}>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField
                id="fname"
                label="First Name"
                variant="outlined"
                value={fname}
                onChange={(e) => setFname(e.target.value)}
                fullWidth
                required
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                id="lname"
                label="Last Name"
                variant="outlined"
                value={lname}
                onChange={(e) => setLname(e.target.value)}
                fullWidth
                required
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                id="username"
                label="Username"
                variant="outlined"
                value={username}
                onChange={(e) => setUsername(e.target.value)}
                fullWidth
                required
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                id="email"
                label="Email"
                variant="outlined"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                fullWidth
                required
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                id="avatar"
                label="Avatar"
                variant="outlined"
                value={avatar}
                onChange={(e) => setAvatar(e.target.value)}
                fullWidth
              />
            </Grid>
            <Grid item xs={12}>
              <Button type="submit" variant="contained" fullWidth>
                Update
              </Button>
            </Grid>
          </Grid>
        </form>
      </Container>
    </React.Fragment>
  );
}
