import React,{useState,useEffect} from 'react';
import CssBaseline from '@mui/material/CssBaseline';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import "./Card.css"
import Link from '@mui/material/Link';




export default function SimpleContainer() {
    const [itemsCard, setItemscard] = useState([]);

    useEffect(() => {
        UserGet()
      }, [])
      const UserGet = () =>{
        fetch("https://www.melivecode.com/api/users")
          .then(res => res.json())
          .then(
            (result) => {
              setItemscard(result);
            }
           
          )
      }
      const UserUpdate = id => {
        window.location = '/update/'+id
      }

      const UserDelete = id => {
        var myHeaders = new Headers();
            myHeaders.append("Content-Type", "application/json");

        var raw = JSON.stringify({
            "id": id
        });

        var requestOptions = {
        method: 'DELETE',
        headers: myHeaders,
        body: raw,
        redirect: 'follow'
        };

        fetch("https://www.melivecode.com/api/users/delete", requestOptions)
        .then(response => response.json())
        .then(result => {
            alert(result['message'])
            if (result['status'] === 'ok'){
                UserGet()
            }
        })
        .catch(error => console.log('error', error));
            }
  return (
    <React.Fragment>
      <CssBaseline />
      <Container maxWidth="lg" sx={{p:7 }}>
      <Box display="flex">
            <Box sx={{ flexGrow:1 , p:2}}>
            <Typography variant="h6" gutterBottom>
                User
            </Typography>
            </Box>
            <Box>
                <Link href="create">
                    <Button  variant="contained">create</Button>
                </Link>
            </Box>
        </Box>
      <Box
              sx={{
                display: "grid",
                gridTemplateColumns: "repeat(auto-fill, minmax(320px, 1fr))",
                gap: "16px"
              }}
            >
                { itemsCard.map ((card)=>(
                    <Card className='container' >
                    <CardMedia className='pic'
                        image={card.avatar }
                        title={card.fname}
                    />
                    <CardContent className='data'>
                        <Typography gutterBottom variant="h5" component="div" >
                        <h5 >ID: {card.id} {card.fname} </h5> </Typography>
                        <Typography variant="body2" color="text.secondary" sx={{margin:1}}>
                        <p>Name:  {card.fname} {card.lname}</p>
                        <p>E-mail : {card.username}</p>
                        </Typography>
                    </CardContent>
                    <CardActions className='button-Group'  >
                        <Button  className='buttonCard' variant="contained"  onClick={() => UserUpdate(card.id)}>Edit</Button>
                        <Button className='buttonCard' variant="contained" onClick={() => UserDelete(card.id)}>Delete</Button>
                    </CardActions>
                    </Card>
                ))}
            </Box>
        
                
                
            
            
            
        
        
        
        
      </Container>
    </React.Fragment>
  );
}