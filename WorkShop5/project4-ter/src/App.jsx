import "./App.css";
import Navbar from "./components/Navbar";
import User from "./components/User";
import { Route, Routes,BrowserRouter } from "react-router-dom";
import Usercreate from "./components/Usercreate";
import Userupdate from "./components/Userupdate";
function App() {
  return (
    <div>
      <BrowserRouter>
        <Navbar />
        <Routes>
          <Route path="/" element={<User />} />
          <Route path="create" element={<Usercreate />} />
          <Route path="update/:id" element={<Userupdate />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
