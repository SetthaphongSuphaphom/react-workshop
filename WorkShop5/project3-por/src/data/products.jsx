const products =[
    {
        id:1,
        name:"หมวก",
        image:"assets/product1.png",
        price:2000,
        quantity:1
    },
    {
        id:2,
        name:"หูฟัง",
        image:"assets/product2.png",
        price:1500,
        quantity:1
    },
    {
        id:3,
        name:"กระเป๋า",
        image:"assets/product3.png",
        price:1000,
        quantity:1
    }
]
export default products