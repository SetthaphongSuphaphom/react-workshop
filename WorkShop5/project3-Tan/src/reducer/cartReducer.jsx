const CartReducer = (state, action) => {
      //กระบวนการจัดการ State
      if (action.type === "CALCULATE_TOTAL") {
        const { total, amount } = state.products.reduce(
          (cartTotal, item) => {
            const { price, quantity } = item;
            const totalprice = price * quantity;//ยอดรวมสินค้า
            cartTotal.total += totalprice; //เงินรวม
            cartTotal.amount += quantity; //ปริมาณรวม
            return cartTotal;
          },
          {
            total: 0,
            amount: 0,
          }
        );

        return { ...state, total, amount };
      }
      if (action.type === "REMOVE") {
        return {
          ...state,
          products: state.products.filter((item) => item.id !== action.payload),
        };
      }
      if (action.type === "ADD") { //เพิ่มสินค้าพร้อม ราคาสินค้า
        let updateProduct = state.products.map((item) => {
          if (item.id === action.payload) {
            return {
              ...item,
              quantity: item.quantity + 1,
            };
          }
          return item;
        });
        return {
          ...state,
          products: updateProduct,
        };
      }
      if (action.type === "Delete") {//ลดสินค้าพร้อม ราคาสินค้า
        let updateProduct = state.products
          .map((item) => {
            if (item.id === action.payload) {
              return {
                ...item,
                quantity: item.quantity - 1,
              };
            }
            return item;
          })
          .filter((item) => item.quantity !== 0);
        return {
          ...state,
          products: updateProduct,
        };
      }
    };
    export default CartReducer;
    