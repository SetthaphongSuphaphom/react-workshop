import "./Header.css"
import { useCart } from "../context/CartContext"
export default function Header(){
    const {amount} = useCart()
    return(
        <header>
            <p>Shopping Application</p>
            <p>สินค้าในตะกร้า : 0</p>
        </header>
    )
}