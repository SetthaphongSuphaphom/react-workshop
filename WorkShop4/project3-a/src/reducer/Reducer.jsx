import React from "react";

const Reducer = (state, action) => {
  if (action.type === "CALCULATE_TOTAL") {
    const { total, amount } = state.product.reduce(
      (cartTotal, item) => {
        const { price, quantity } = item;
        // console.log(price);
        // console.log(quantity);
        const totalPrice = price * quantity;
        cartTotal.total += totalPrice;
        cartTotal.amount += quantity;
        return cartTotal;
      },
      {
        total: 0,
        amount: 0,
      }
    );
    return {
      ...state,
      total,
      amount,
    };
  } else if (action.type === "REMOVE_ITEM") {
    const resultRemove = state.product.filter((data) => data.id !== action.id);
    return {
      ...state,
      product: resultRemove,
    };
  } else if (action.type === "INCREASE_ITEM") {
    let result = state.product.map((data) => {
      if (data.id === action.id) {
        return {
          ...data,
          quantity: data.quantity + 1,
        };
      }
      return data;
    });
    console.log(result);
    return {
      ...state,
      product: result,
    };
  } else if (action.type === "DECREASE_ITEM") {
    let result = state.product
      .map((data) => {
        if (data.id === action.id) {
          return {
            ...data,
            quantity: data.quantity - 1,
          };
        }
        return data;
      })
      .filter((item) => item.quantity !== 0);

    return {
      ...state,
      product: result,
    };
  }
};

export default Reducer;
