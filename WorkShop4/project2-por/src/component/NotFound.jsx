import notFound from "../pic/notFound.webp"
export default function NotFound(){
    return(
        <div className="container">
            <h3 className="title">ไม่พบหน้าเว็บ(404 Page Not ound)</h3>
            <img src={notFound} alt="notfound" />
        </div>
    );
}