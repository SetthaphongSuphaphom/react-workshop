import { useState } from "react";
import reactLogo from "./assets/react.svg";
import viteLogo from "/vite.svg";
import Header from "./components/Header";
import Cart from "./components/Cart";
import "./App.css";

function App() {
  return (
    <div className="App">
      <Header />
      <Cart/>
    </div>
  );
}

export default App;
