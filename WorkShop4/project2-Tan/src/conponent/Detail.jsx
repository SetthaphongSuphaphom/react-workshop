import { useParams } from "react-router-dom";
import { useEffect, useState } from "react";
import blogs from "../data/blogs.jsx";
import "./Detail.css";

export default function Details(){
    const {id} = useParams()
    const [title ,setTitle] = useState('');
    const [Image,setImg] = useState('');
    const [content,setContent] = useState('');
    const [author,setAuther] = useState('');

    useEffect(()=>{
        const result = blogs.find((item)=>(item.id === parseInt(id)))
        setTitle(result.title)
        setImg(result.image_url)
        setContent(result.content)
        setAuther(result.author)
        //eslint-disable-next-line
    },[])
    return(
        <>
        <div className="container">
            <h2 className="title">บทความ : {title}</h2>
            <img src={Image} alt={title} className="blog-image"/>
            <div className="blog-detail">
                <strong>ผู้เขียน : {author}</strong>
                <p>{content}</p>
            </div>
        </div>
        </>
    );
}