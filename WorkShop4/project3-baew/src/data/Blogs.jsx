const Blogs=[
    {
        id:1,
        title:"รู้จักกับส้มตำไทย",
        image_url:"https://img.kapook.com/u/2015/wanwanat/9979_Somtum/somtum.jpg",
        content: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Officiis excepturi architecto aut, similique dolorem voluptatum minus dolores magnam alias illum odio sequi voluptas, itaque perspiciatis earum sapiente, ipsa harum! Optio. Lorem ipsum dolor sit amet consectetur, adipisicing elit. Neque enim excepturi non officiis molestias odit quam? Distinctio perferendis deserunt omnis, eaque delectus consectetur dolore tempore dolorem! Numquam eum accusamus hic.e" ,
        author: "แบ๋ว"
    },
    {
        id:2,
        title:"ข้าวต้มผัด",
        image_url:"https://img.wongnai.com/p/1920x0/2019/01/06/fd63cb4dfdd84302a9c53c96a1d7a9b8.jpg",
        content: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Officiis excepturi architecto aut, similique dolorem voluptatum minus dolores magnam alias illum odio sequi voluptas, itaque perspiciatis earum sapiente, ipsa harum! Optio. Lorem ipsum dolor sit amet consectetur, adipisicing elit. Neque enim excepturi non officiis molestias odit quam? Distinctio perferendis deserunt omnis, eaque delectus consectetur dolore tempore dolorem! Numquam eum accusamus hic.e",
        author: "แบม"
    },
    {
        id:3,
        title:"ยำทะเล",
        image_url:"https://f.ptcdn.info/382/069/000/qa99yo8rysNZHKRaUjKc-o.jpg",
        content: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Officiis excepturi architecto aut, similique dolorem voluptatum minus dolores magnam alias illum odio sequi voluptas, itaque perspiciatis earum sapiente, ipsa harum! Optio.Lorem ipsum dolor sit amet consectetur, adipisicing elit. Neque enim excepturi non officiis molestias odit quam? Distinctio perferendis deserunt omnis, eaque delectus consectetur dolore tempore dolorem! Numquam eum accusamus hic.e",
        author: "ปอ"
    },
    {
        id:4,
        title:"พระอาทิตย์ตกดิน",
        image_url:"https://img.kapook.com/u/patcharin/Variety/Sun/sunset.jpg",
        content: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Officiis excepturi architecto aut, similique dolorem voluptatum minus dolores magnam alias illum odio sequi voluptas, itaque perspiciatis earum sapiente, ipsa harum! Optio. Lorem ipsum dolor sit amet consectetur, adipisicing elit. Neque enim excepturi non officiis molestias odit quam? Distinctio perferendis deserunt omnis, eaque delectus consectetur dolore tempore dolorem! Numquam eum accusamus hic.e",
        author: "มูมู"
    },
    {
        id:5,
        title:"เที่ยวทะเล",
        image_url:"https://talaythailand.com/wp-content/uploads/2022/06/image-15.png",
        content: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Officiis excepturi architecto aut, similique dolorem voluptatum minus dolores magnam alias illum odio sequi voluptas, itaque perspiciatis earum sapiente, ipsa harum! Optio.Lorem ipsum dolor sit amet consectetur, adipisicing elit. Neque enim excepturi non officiis molestias odit quam? Distinctio perferendis deserunt omnis, eaque delectus consectetur dolore tempore dolorem! Numquam eum accusamus hic.e",
        author: "บู้บี้"
    },
]
export default Blogs;