import Blogs from "../data/Blogs";
import "./Blog.css";
import { Link } from "react-router-dom";
import { useState, useEffect } from "react";

export default function Blog() {
  const [search, setSearch] = useState("");
  const [filterBlog,setFilterBlog] = useState([]);
  useEffect(()=>{
    //กรองข้อมูลชื่อบทความ
    const result = Blogs.filter((item)=>item.title.includes(search))
    setFilterBlog(result)
  },[search])

  return (
    <div className="blogs-container">
      <article>
        <div className="search-container">
          <input
            type="text"
            className="search-input"
            placeholder="ค้นหาบทความ"
            value={search}
            onChange={(e) => setSearch(e.target.value)}
          />
        </div>
        {filterBlog.map((item) => (
          <Link to={`/blogs/${item.id}`} key={item.id}>
            <div className="card" key={item.id}>
              <h2>{item.title}</h2>
              <p>{item.content.substring(0, 300)}</p>
              <hr />
            </div>
          </Link>
        ))}
      </article>
    </div>
  );
}
