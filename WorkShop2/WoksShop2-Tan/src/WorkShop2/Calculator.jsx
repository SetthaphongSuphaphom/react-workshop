import React, { useState } from "react";
import "./ws2.css";

function CalculatorForm() {
  const [num1, setNum1] = useState('');
  const [num2, setNum2] = useState('');
  const [operator, setOperator] = useState('');
  const [result, setResult] = useState('');

  const handleInputChange = (event) => {
    const name = event.target.name;
    const value = event.target.value;

    if (name === 'num1') {
      setNum1(value);
    } else if (name === 'num2' ) {
      setNum2(value);
    }
  };

  const handleOperatorChange = (event) => {
    setOperator(event.target.value);
  };

  const handleCalculate = (event) => {
    event.preventDefault();

    const n1 = parseFloat(num1);
    const n2 = parseFloat(num2);

    if (isNaN(n1) || isNaN(n2)) {
      setResult('Invalid input');
      return;
    }

    let res = 0;
    switch (operator) {
      case '+':
        res = n1 + n2;
        break;
      case '-':
        res = n1 - n2;
        break;
      case '*':
        res = n1 * n2;
        break;
      case '/':
        if (n2 === 0) {
          setResult('Cannot divide by zero');
          return;
        }
        res = n1 / n2;
        break;
      default:
        setResult('Invalid operator');
        return;
    }

    setResult(res.toString());
  };

  const handleClear = () => {
    setNum1('');
    setNum2('');
    setOperator('');
    setResult('');
  };

  return (
    <form className="calculator-form" onSubmit={handleCalculate}>
      <div className="form-group">
        <label htmlFor="num1">Number 1:</label>
        <input
          type="text"
          id="num1"
          name="num1"
          value={num1}
          onChange={handleInputChange}
          required
        />
      </div>
      <div className="form-group">
        <label htmlFor="num2">Number 2:</label>
        <input
          type="text"
          id="num2"
          name="num2"
          value={num2}
          onChange={handleInputChange}
          required
        />
      </div>
      <div className="form-group">
        <label htmlFor="operator">Operator:</label>
        <select id="operator" name="operator" value={operator} onChange={handleOperatorChange}>
          <option value="">Select an operator</option>
          <option value="+">+</option>
          <option value="-">-</option>
          <option value="*">*</option>
          <option value="/">/</option>
        </select>
      </div>
      <div className="form-group">
        <button type="submit">Calculate</button>
        <button type="button" onClick={handleClear}>Clear</button>
      </div>
      {
        
      result && <div className="result">{result}
      </div>
      }
    </form>
  );
}

export default CalculatorForm;