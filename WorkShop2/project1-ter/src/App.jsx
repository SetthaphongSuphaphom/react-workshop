import { useState,useEffect } from "react";
import "./App.css";
import Navbar from "./components/Header";
import Form from "./components/Form";
import Item from "./components/Item";
function App() {
  const [tasks, setTasks] = useState(JSON.parse(localStorage.getItem("tasks")) ||[]);
  const [title, setTitle] = useState("");
  const [updateid, setUpdateId] = useState(null);
  const [theme,setTheme] = useState("light")
  useEffect(()=>{
    localStorage.setItem("tasks",JSON.stringify(tasks))
  },[tasks])

  function deleteTask(id) {
    const result = tasks.filter((item) => item.id !== id);
    setTasks(result);
    console.log(result);
  }
  function updateTask(id) {
    setUpdateId(id);
    const updateTask = tasks.find((item) => item.id === id);
    console.log(updateTask);
    setTitle(updateTask.title);
  }
  function saveTask(e) {
    e.preventDefault();
    if (!title) {
      alert("กรุณาป้อนข้อมูล");
    } else if (updateid) {
      const saveTask = tasks.map((item) => {
        if (item.id === updateid) {
          return { ...item, title: title };
        }
        return item;
      });
      console.log(saveTask);
      setTasks(saveTask);
      setUpdateId(null);
      setTitle("");
    } else {
      const newTask = {
        id: Math.floor(Math.random() * 1000),
        title: title,
      };
      setTasks([...tasks, newTask]);
      setTitle("");
    }
    console.log("save");
  }
  return (
    <div className={"App "+theme}>
      <Navbar theme={theme} setTheme={setTheme}/>
      <div className="container">
        <Form
          title={title}
          setTitle={setTitle}
          saveTask={saveTask}
          updateid={updateid}
        />
        <section>
          {tasks.map((data) => (
            <Item
              key={data.id}
              data={data}
              deleteTask={deleteTask}
              updateTask={updateTask}
            />
          ))}
        </section>
      </div>
    </div>
  );
}

export default App;
