import React, { useState } from "react";
import "../components/StudentList.css";
import Item from "./Item";
React;

const StudentList = ({ students }) => {
  const [show, setShow] = useState(true);
  const myStyle = {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  };

  return (
    <div style={myStyle} className="header-container">
      <ul>
        <div className="header">
          <h1 style={{ color: "greenyellow", fontSize: "20px" }}>
            จำนวนนักเรียน = ( {students.length} )
          </h1>
          <button
            onClick={() => setShow(!show)}
            style={
              show ? { backgroundColor: "red" } : { backgroundColor: "green" }
            }
          >
            {show ? "Hide" : "Show"}
          </button>
        </div>

        {show &&
          students.map((data) => (
            <Item key={data.id} data={data} /> //หรือจะส่งเป็น <Item key={data.id} name={data.name} id={data.id}/> ก็ได้
          ))}
      </ul>
    </div>
  );
};

export default StudentList;
