import React, { useContext, useState } from "react";
import "../components/AddFrom.css";
import { studentsContext } from "../App";
const AddFrom = () => {
  const [name, setName] = useState("");
  const { students, setStudent } = useContext(studentsContext);
  const [gender, setGender] = useState("male");

  const saveStudent = (event) => {
    event.preventDefault();
    try {
      if (!name) {
        alert("Please input name");
      } else {
        const newStudent = {
          id: Math.floor(Math.random() * 3) + 1,
          name: name,
          gender: gender,
        };
        console.log(newStudent.id);

        for (let i = 0; i < students.length; i++) {
          if (students[i].id === newStudent.id) {
            const newRandom = Math.floor(Math.random() * 1000) + 1;
            newStudent.id = newRandom;
            console.log("ข้อมูลในการ random id ตัวใหม่" + newStudent);
          }
        }
        setStudent([...students, newStudent]);
        console.log("ข้อมูลนักเรียน" + newStudent);
        setName("");
        setGender("male");
      }
    } catch (error) {
      console.error("error" + error.message);
    }
  };

  return (
    <section className="add-form-container">
      <form onSubmit={saveStudent}>
        <label htmlFor="">ชื่อนักเรียน</label>
        <input
          type="text"
          value={name}
          name="name"
          onChange={(e) => setName(e.target.value)}
        />
        <select value={gender} onChange={(e) => setGender(e.target.value)}>
          <option value="male">ชาย</option>
          <option value="female">หญิง</option>
        </select>
        <button type="submit" className="btn-add">
          บันทึก
        </button>
      </form>
    </section>
  );
};

export default AddFrom;
