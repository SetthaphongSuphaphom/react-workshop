import logo from "../image/logo.png";

export default function Header({title}){
    return(
        <nav>
            <img src={logo} alt="Logo" className="logo"/>
            <a href="/">{title}</a>
        </nav>
    );
}
