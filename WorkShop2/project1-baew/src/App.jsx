import { useState } from "react";
import reactLogo from "./assets/react.svg";
import viteLogo from "/vite.svg";
import "./App.css";
import "./components/Header.css";
import Header from "./components/Header";
import StudentList from "./components/StudentList";
import "./components/StudentList.css";
import AddForm from "./components/Form";
import "./components/Form.css";
import "./components/Item.css";

function App() {
  const [students, setStudent] = useState([
  ]);

  function deleteStudent(id) {
    setStudent(students.filter((item) => item.id !== id));
  }

  return (
    <div className="App">
      <Header title="Home" />
      <main>
        <AddForm students={students} setStudent={setStudent}/>
        <StudentList students={students} deleteStudent={deleteStudent}/>
      </main>
    </div>
  );
}

export default App;
