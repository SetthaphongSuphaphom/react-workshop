import "./Item.css";
import { BiTrash, BiEdit } from "react-icons/bi";

function Item(props) {
  const { data, deleteTask, editTask } = props;
  return (
    <div className="list-item">
      <p className="title">{data.title}</p>
      <div className="button-container">
        <BiTrash className="btn" onClick={() => deleteTask(data.id)}></BiTrash>
        <BiEdit className="btn" onClick={() => editTask(data.id)}></BiEdit>
      </div>
    </div>
  );
}

export default Item;
