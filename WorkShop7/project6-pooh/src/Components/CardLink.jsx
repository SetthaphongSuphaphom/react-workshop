import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import "./CC.css";
import Link from "@mui/material/Link";
import { useState, useEffect } from "react";
import Masonry from "@mui/lab/Masonry";

export default function CardLink(props) {
  const { lo, ra, pr, im } = props;

  const [rate, setRate] = useState("");
  const [location, setLocation] = useState("");
  const [price, setPrice] = useState("");
  const [image, setImage] = useState("");

  useEffect(() => {
    setLocation(lo);
    setPrice(pr);
    setRate(ra);
    setImage(im);
  }, []);

  return (
    <Link href="" sx={{ textDecoration: "none" }}>
      <Box
        sx={{
          borderRadius: "1.5rem",
          margin: "auto",
          backgroundImage: `url(${image})`,
          // "url(https://images.unsplash.com/photo-1551963831-b3b1ca40c98e)",
          backgroundSize: "cover",
          width: "100%",
          height: "200px",
          filter: "blur(0.5px)",
          color: "white",
          position: "relative"
        }}
      >
        <Box
          sx={{
            backgroundColor: "rgba(0,0,0, 0.05)",
            backgroundColor: "rgb(0,0,0,0.2)",
            color: "white",
            width: "100%",
            height: "100%",
            borderRadius: "1.5rem",
            padding: "15%",
            display: "flex",
            justifyContent: "space-between",
            justifyItems: "space-evenly",
            position: "relative",
          }}
        >
          <Typography sx={{ fontSize: "2.1vh", fontWeight: "bold" }}>
            {location}
          </Typography>
          <Box
            sx={{
              position: "absolute",
              right:"0",
              top:"0",
              width: "25%",
              height: "22%",
              borderRadius: "0rem 1.5rem",
              backgroundColor: "#FDCC49",
              color: "#805605",
              padding:"7%",
              // fontSize:"3vw"
            }}
          >
            <Typography sx={{textAlign:"center",fontWeight: "bold",fontSize:"2vh",position:"absolute"}}>
            {rate}
            </Typography>

          </Box>
        </Box>
        <Box sx={{}}>
          <Typography
            sx={{
              position: "relative",
              fontSize: "3vh",
              marginLeft: "15%",
              fontWeight: "bold",
              marginTop: "-32%",
              maxWidth:"300px",
            }}
          >
            ${price}
            <Typography sx={{ fontSize: "1.5vh", fontWeight: "light" }}>
              per night
            </Typography>
          </Typography>
        </Box>
      </Box>
    </Link>
  );
}
