import { Box, IconButton, Button , Link} from "@mui/material";
import FlightIcon from "@mui/icons-material/Flight";
import "./BookFlight.css";
import CalendarTodayOutlinedIcon from "@mui/icons-material/CalendarTodayOutlined";
import PersonIcon from "@mui/icons-material/Person";
import FlightClassIcon from "@mui/icons-material/FlightClass";


function BookFlight() {
  return (
    <>
      <Box className="Container" sx={{ width: "100%" }}>
        <div className="header">
          <img className="img-plane" src="../public/plane.png" alt="plane" />
          <h1 className="txt-header">
            Book Your <br />
            Flight
          </h1>
        </div>
        <div className="sub-container">
          <div className="btn-group">
            <Button className="btn active" variant="contained">
              Round Trip
            </Button>
            <Button className="btn" variant="contained">
              Oneway
            </Button>
          </div>
          <Box className="content">
            <span>From</span>
            <div className="box">
              <IconButton sx={{ color: "#02315E", transform: "rotate(50deg)" }}>
                <FlightIcon />
              </IconButton>
              <p>Paris</p>
            </div>
            <span>To</span>
            <div className="box">
              <IconButton
                sx={{ color: "#02315E", transform: "rotate(120deg)" }}
              >
                <FlightIcon />
              </IconButton>
              <p>Florence</p>
            </div>
            <div className="flex-box">
              <div style={{ width: "50%" }}>
                <span>Deport</span>
                <br />
                <div className="box">
                  <IconButton sx={{ color: "#02315E" }}>
                    <CalendarTodayOutlinedIcon />
                  </IconButton>
                  <p>24 January</p>
                </div>
              </div>
              <div style={{ width: "50%" }}>
                <span>Return</span>
                <br />
                <div className="box">
                  <IconButton sx={{ color: "#02315E" }}>
                    <CalendarTodayOutlinedIcon />
                  </IconButton>
                  <p style={{ color: "#02315E" }}>31 January</p>
                </div>
              </div>
            </div>
            <div className="flex-box">
              <div style={{ width: "100%" }}>
                <span>Passenger</span>
                <br />
                <div className="box">
                  <IconButton sx={{ color: "#02315E" }}>
                    <PersonIcon />
                  </IconButton>
                  <p>2</p>
                </div>
              </div>
              <div className="flex-box">
                <div style={{ width: "100%" }}>
                  <span>Class</span>
                  <br />
                  <div className="box">
                    <IconButton sx={{ color: "#02315E" }}>
                      <FlightClassIcon />
                    </IconButton>
                    <p style={{ color: "#02315E" }}>First</p>
                  </div>
                </div>
              </div>
            </div>
            <Link href="/selectflight" sx={{textDecoration:"none"}}>
              <Button className="btn-search"  variant="contained">
                Search Flights
              </Button>
            </Link>
          </Box>
        </div>
      </Box>
    </>
  );
}

export default BookFlight;
