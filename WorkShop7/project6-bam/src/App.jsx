import "./App.css";
import BookFlight from "./components/BookFlight";
import SelectFlight from "./components/SelectFlight";
import { BrowserRouter as Router, Routes, Route, Link  } from "react-router-dom";


function App() {
  return (
    <div>
      <Router>
        {/* <BookFlight /> */}
        {/* <SelectFlight /> */}
        <Routes>
          <Route path="/" element={<BookFlight />} />
          <Route path="selectflight" element={<SelectFlight />} />
        </Routes>
      </Router>
    </div>
  );
}

export default App;
