import React, { useState } from "react";
import CssBaseline from "@mui/material/CssBaseline";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import { styled } from "@mui/material/styles";
import Paper from "@mui/material/Paper";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import ButtonBase from "@mui/material/ButtonBase";
import Data from "./Data";
import { useTheme } from "@mui/material/styles";

const Img = styled("img")({
  margin: "auto",
  display: "block",
  maxWidth: "100%",
  maxHeight: "100%",
});

const InAndOut = () => {
  const theme = useTheme();
  return (
    <React.Fragment>
      <CssBaseline />
      <Container
        sx={{
          bgcolor: "#f5f6fb",
          p: 3,
          flexGrow: 1,
          mt: -11,
          [theme.breakpoints.up("sm")]: {
            borderRadius: "0",
            mt: 0,
            maxWidth: "lg",
            p: 0,
            mb: "200px",
          },
        }}
      >
        {Data.map((data) => (
          <Box
            sx={{
              height: "100%",
              position: "relative",
              zIndex: 2,
              mb: 2,
              bgcolor: "white",
              borderRadius: "3px",
              pb: 2,
              [theme.breakpoints.up("sm")]: {
                position: "unset",
                zIndex: 0,
                mt: 10,
                height: "30%",
                px: 10,
              },
            }}
            key={data.id}
          >
            <Box
              sx={{
                height: "40px",
                borderBottom: "1px solid #f5f6fb",
                display: "flex",
                alignItems: "center",
                py: 1,
                px: 2,
                [theme.breakpoints.up("sm")]: {
                  height: "100px",
                },
              }}
            >
              <Typography
                variant="h6"
                noWrap
                component="a"
                href="/"
                sx={{
                  color: "#52595D",
                  letterSpacing: ".1rem",
                  textDecoration: "none",
                  fontSize: "1.4rem",
                  fontWeight: 600,
                  [theme.breakpoints.up("sm")]: {
                    fontSize: "24px",
                  },
                }}
              >
                {data.name}
              </Typography>
            </Box>
            {data.day.map((data) => (
              <Box key={data.id}>
                <Paper
                  sx={{
                    flexGrow: 1,
                    color: "black",
                    display: "flex",
                    justifyContent: "center",
                    borderBottom: "1px solid #f5f6fb",
                    boxShadow: 0,
                  }}
                >
                  <Box
                    sx={{
                      flexGrow: 1,
                      display: "flex",
                      justifyContent: "space-between",
                      alignItems: "center",
                      py: 1,
                      px: 2,
                      borderRadius: "10px",
                      [theme.breakpoints.up("sm")]: {
                        px: 4,
                      },
                    }}
                  >
                    <Grid item>
                      <ButtonBase
                        sx={{
                          width: 50,
                          height: 50,
                          [theme.breakpoints.up("sm")]: {
                            width: 100,
                            height: 100,
                          },
                        }}
                      >
                        <Img alt={data.fname} src={data.avatar} />
                      </ButtonBase>
                    </Grid>
                    <Grid
                      item
                      xs={7}
                      sm
                      container
                      sx={{
                        [theme.breakpoints.up("sm")]: {
                          ml: 4,
                        },
                      }}
                    >
                      <Grid item xs container>
                        <Grid item xs>
                          <Typography
                            gutterBottom
                            variant="subtitle1"
                            component="div"
                            sx={{
                              fontSize: "1.7rem",
                              fontWeight: 700,
                              color: "#52595D",
                              [theme.breakpoints.up("sm")]: {
                                fontSize: "27px",
                              },
                            }}
                          >
                            {data.fname} {data.lname}
                          </Typography>
                          <Typography
                            sx={{
                              fontWeight: 700,
                              cursor: "pointer",
                              fontSize: "1.4rem",
                              color:
                                data.id % 2 === 0 ? "lightgreen" : "orange",
                              [theme.breakpoints.up("sm")]: {
                                fontSize: "22px",
                              },
                            }}
                            variant="body2"
                          >
                            {data.id % 2 === 0 ? "sent" : "Received"}
                          </Typography>
                        </Grid>
                      </Grid>
                    </Grid>
                    <Box
                      sx={{
                        display: "flex",
                        flexDirection: "column",
                        justifyContent: "space-around",
                        alignItems: "center",
                        height: "100%",
                      }}
                    >
                      <Grid item>
                        <Typography
                          variant="subtitle1"
                          component="div"
                          sx={{
                            fontSize: "1.7rem",
                            fontWeight: 700,
                            color: "#34282C",
                            [theme.breakpoints.up("sm")]: {
                              fontSize: "27px",
                            },
                          }}
                        >
                          $19.00
                        </Typography>
                      </Grid>
                      <Grid item></Grid>
                    </Box>
                  </Box>
                </Paper>
              </Box>
            ))}
          </Box>
        ))}
      </Container>
    </React.Fragment>
  );
};

export default InAndOut;
