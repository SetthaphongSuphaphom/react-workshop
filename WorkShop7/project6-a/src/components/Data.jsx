const Data = [
  {
    id: 1,
    day: [
      {
        id: 1,
        fname: "Karn",
        lname: "Yong",
        username: "karn.yong@melivecode.com",
        avatar: "https://www.melivecode.com/users/1.png",
      },
      {
        id: 2,
        fname: "Ivy",
        lname: "Cal",
        username: "ivy.cal@melivecode.com",
        avatar: "https://www.melivecode.com/users/2.png",
      },
      {
        id: 3,
        fname: "Walter",
        lname: "Beau",
        username: "walter.beau@melivecode.com",
        avatar: "https://www.melivecode.com/users/3.png",
      },
    ],
    name: "sunday",
  },
  {
    id: 2,
    day: [
      {
        id: 1,
        fname: "Gayla",
        lname: "Bertrand",
        username: "gayla.bertrand@melivecode.com",
        avatar: "https://www.melivecode.com/users/4.png",
      },
    ],
    name: "monday",
  },
  {
    id: 3,
    day: [
      {
        id: 1,
        fname: "Benjamin",
        lname: "Chaz",
        username: "benjamin.chaz@melivecode.com",
        avatar: "https://www.melivecode.com/users/5.png",
      },

      {
        id: 2,
        fname: "Delia",
        lname: "Robin",
        username: "delia.robin@melivecode.com",
        avatar: "https://www.melivecode.com/users/6.png",
      },
      {
        id: 3,
        fname: "Hector",
        lname: "Graves",
        username: "hector.graves@melivecode.com",
        avatar: "https://www.melivecode.com/users/7.png",
      },
      {
        id: 4,
        fname: "Diego",
        lname: "Greene",
        username: "diego.greene@melivecode.com",
        avatar: "https://www.melivecode.com/users/8.png",
      },
      {
        id: 5,
        fname: "Izzie",
        lname: "Mooney",
        username: "izzie.mooney@melivecode.com",
        avatar: "https://www.melivecode.com/users/9.png",
      },
      {
        id: 6,
        fname: "Romeo",
        lname: "Garner",
        username: "romeo.garner@melivecode.com",
        avatar: "https://www.melivecode.com/users/10.png",
      },
      {
        id: 7,
        fname: "Adrian",
        lname: "Faisal",
        username: "adrian.faisal@melivecode.com",
        avatar: "https://www.melivecode.com/users/11.png",
      },
      {
        id: 8,
        fname: "Katarina",
        lname: "Aba",
        username: "katarina.aba@melivecode.com",
        avatar: "https://www.melivecode.com/users/12.png",
      },
    ],
    name: "tuesday",
  },
];

export default Data;
