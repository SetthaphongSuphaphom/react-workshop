import * as React from "react";
import { styled } from "@mui/material/styles";
import BottomNavigation from "@mui/material/BottomNavigation";
import BottomNavigationAction from "@mui/material/BottomNavigationAction";
import RestoreIcon from "@mui/icons-material/Restore";
import FavoriteIcon from "@mui/icons-material/Favorite";
import Box from "@mui/material/Box";
import HomeIcon from "@mui/icons-material/Home";
import PersonIcon from "@mui/icons-material/Person";
import { Link } from "react-router-dom";
import { useTheme } from "@mui/material/styles";

const StyledBottomNavigation = styled(BottomNavigation)({
  width: "100%",
  position: "fixed",
  bottom: 0,
  left: 0,
  right: 0,
  zIndex: 3,
});

const BottomBar = () => {
  const theme = useTheme();
  return (
    <Box sx={{}}>
      <StyledBottomNavigation
        sx={{
          bgcolor: "white",
          py: 0,
          borderWidth: "5px 5px 5px 5px",
          borderColor: "#f5f6fb",
          borderStyle: "solid",
          borderRadius: "50px 50px 50px 50px",
          height: "10%",
          [theme.breakpoints.up("sm")]: {
            borderRadius: 0,
            borderWidth: 0,
            borderStyle: "unset",
            height: "100px",
            display: "flex",
            justifyContent: "space-around",
            p: 5,
          },
        }}
        showLabels
      >
        <BottomNavigationAction
          component={Link}
          to="/"
          sx={{ flexGrow: 1 }}
          icon={
            <HomeIcon
              sx={{
                width: "3rem",
                height: "3rem",
                [theme.breakpoints.up("sm")]: {
                  height: "48px",
                  width: "48px",
                },
              }}
            />
          }
        />

        <BottomNavigationAction
          component={Link}
          to="/inAndOut"
          sx={{ flexGrow: 1 }}
          icon={
            <RestoreIcon
              sx={{
                width: "3rem",
                height: "3rem",
                [theme.breakpoints.up("sm")]: {
                  height: "48px",
                  width: "48px",
                },
              }}
            />
          }
        />

        <BottomNavigationAction
          sx={{ flexGrow: 1 }}
          icon={
            <FavoriteIcon
              sx={{
                width: "3rem",
                height: "3rem",
                [theme.breakpoints.up("sm")]: {
                  height: "48px",
                  width: "48px",
                },
              }}
            />
          }
        />
        <BottomNavigationAction
          sx={{ flexGrow: 1 }}
          icon={
            <PersonIcon
              sx={{
                width: "3rem",
                height: "3rem",
                [theme.breakpoints.up("sm")]: {
                  height: "48px",
                  width: "48px",
                },
              }}
            />
          }
        />
      </StyledBottomNavigation>
    </Box>
  );
};

export default BottomBar;
