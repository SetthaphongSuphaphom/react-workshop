import * as React from "react";
import CssBaseline from "@mui/material/CssBaseline";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import { Typography, Avatar, AvatarGroup, Link } from "@mui/material";
import IconButton from "@mui/material/IconButton";
import MoreHorizIcon from "@mui/icons-material/MoreHoriz";
import Chip from "@mui/material/Chip";
import CheckIcon from "@mui/icons-material/Check";
import "./Home.css";
import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
export default function Home() {
  const [checked, setChecked] = useState(false);

  useEffect(() => {
    if (checked) {
      const timeoutId = setTimeout(() => {
        window.location.href = '/task';
      }, 500);
    } else {
      
    }
  }, [checked]);

  return (
    <React.Fragment>
      <CssBaseline />
      <Container
        maxWidth="lg"
        sx={{ p: 2, bgcolor: "#FDF96C", height: "100vh" }}
      >
        <Box display="flex" justifyContent="space-between" sx={{ m: 2 }}>
          <Box sx={{ bgcolor: "black", p: 0.3, borderRadius: "50%" }}>
            <IconButton>
              <KeyboardArrowDownIcon sx={{ color: "white" }} id="drop" />
            </IconButton>
          </Box>
          <Box sx={{ bgcolor: "black", p: 0.3, borderRadius: "50%" }}>
            <IconButton>
              <MoreHorizIcon sx={{ color: "white" }} />
            </IconButton>
          </Box>
        </Box>
        <Chip
          label="Sweet Home"
          variant="outlined"
          sx={{ m: 2, color: "black", border: "1px solid black" }}
        />
        <Box sx={{ m: 2 }}>
          <Typography variant="h2" gutterBottom sx={{ fontWeight: "500" }}>
            Grocery Shopping
          </Typography>
        </Box>
        <Box sx={{ m: 2 }} display="flex" justifyContent="space-between">
          <Box>
            <Typography sx={{ fontSize: "13px", color: "#787878", my: 1 }}>
              Time Left
            </Typography>
            <Typography variant="h4" sx={{ fontWeight: "500px" }}>
              2h 45m
            </Typography>
            <Typography sx={{ fontSize: "13px" }}>Dec 12, 2022</Typography>
          </Box>
          <Box>
            <Typography sx={{ fontSize: "13px", color: "#787878", my: 1 }}>
              Assignee
            </Typography>
            <AvatarGroup max={2}>
              <Avatar
                alt="Eunchae"
                src="https://www.allkpop.com/upload/2023/02/content/030058/1675403924-20230203-hongeunchae.jpg"
              />
              <Avatar
                alt="Chaewon"
                src="https://www.nme.com/wp-content/uploads/2022/04/le-sserafim-kim-chae-won-source-music-girl-group-2022-696x442.jpg"
              />
            </AvatarGroup>
          </Box>
        </Box>
        <Box sx={{ m: 2 }}>
          <Typography sx={{ fontSize: "13px", color: "#787878", my: 3 }}>
            Additional Description
          </Typography>
          <Typography variant="h9">
            We have to buy some fresh bread, fruit, and vegetaables. Supply of
            water is running out.
          </Typography>
        </Box>
        <Box sx={{ m: 2 }}>
          <Typography sx={{ fontSize: "13px", color: "#787878", my: 1 }}>
            Created
          </Typography>
          <Box display="flex" alignItems="center">
            <Typography variant="h9">Dec 10, by Matt </Typography>
            <Avatar
              alt="Eunchae"
              src="https://www.allkpop.com/upload/2023/02/content/030058/1675403924-20230203-hongeunchae.jpg"
              sx={{ ml: 2, width: "20" }}
            />
          </Box>
        </Box>

        <Box
          id="swipe"
          sx={{
            display: "flex",
            justifyContent: "center",
            flexDirection: "column",
          }}
        >
            <Box className="switch">
              <input
                type="checkbox"
                id="switcher"
                checked={checked}
                onChange={() => setChecked(!checked)}
              />
              <label htmlFor="switcher">
                <IconButton>
                  <CheckIcon />
                </IconButton>
              </label>
            </Box>
        </Box>
      </Container>
    </React.Fragment>
  );
}
