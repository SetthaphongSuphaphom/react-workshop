import * as React from "react";
import { styled } from "@mui/material/styles";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import CssBaseline from "@mui/material/CssBaseline";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import IconButton from "@mui/material/IconButton";
import Paper from "@mui/material/Paper";
import Fab from "@mui/material/Fab";
import List from "@mui/material/List";
import AddIcon from "@mui/icons-material/Add";
import DeckIcon from "@mui/icons-material/Deck";
import FreeSolo from "./Search";
import { Grid } from "@mui/material";
import IcecreamOutlinedIcon from "@mui/icons-material/IcecreamOutlined";
import CakeOutlinedIcon from "@mui/icons-material/CakeOutlined";
import BakeryDiningOutlinedIcon from "@mui/icons-material/BakeryDiningOutlined";
import LunchDiningOutlinedIcon from "@mui/icons-material/LunchDiningOutlined";
import Container from "@mui/material/Container";
import { CardActionArea } from "@mui/material";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Rating from '@mui/material/Rating';
import Details from "./Details";
import { Link } from "react-router-dom";



const messages = [{}];

export default function Header() {
  
  return (
    <React.Fragment>
      <CssBaseline />
      <Container maxWidth="lg" style={{boxShadow:"none", border:"none"}}>
        <Box sx={{ bgcolor: "none", height: "100vh" }}>
          <Toolbar sx={{ p: 15, justifyContent: "center" }}>
            <FreeSolo />
          </Toolbar>
          <Paper square sx={{ pb: "50px" }}>
            <List sx={{ mb: 2 }}>
              {messages.map(() => (
                <React.Fragment>
                  <Grid
                    container
                    direction="row"
                    justifyContent="space-between"
                    alignItems="center"
                    style={{ color: "#f50057"}}
                  >
                    <IconButton
                      size="large"
                      edge="start"
                      color="inherit"
                      aria-label="menu"
                      sx={{ mr: 2 }}
                    >
                      <IcecreamOutlinedIcon />
                    </IconButton>
                    <IconButton
                      size="large"
                      edge="start"
                      color="inherit"
                      aria-label="menu"
                      sx={{ mr: 2 }}
                    >
                      <CakeOutlinedIcon />
                    </IconButton>
                    <IconButton
                      size="large"
                      edge="start"
                      color="inherit"
                      aria-label="menu"
                      sx={{ mr: 2 }}
                    >
                      <BakeryDiningOutlinedIcon />
                    </IconButton>
                    <IconButton
                      size="large"
                      edge="start"
                      color="inherit"
                      aria-label="menu"
                      sx={{ mr: 2 }}
                    >
                      <LunchDiningOutlinedIcon />
                    </IconButton>
                  </Grid>
                  <Grid
                    container
                    direction="row"
                    justifyContent="center"
                    alignItems="center"
                    
                  >
                    <Card
                      sx={{ maxWidth: 280 }}
                      style={{ textAlign: "center", marginBottom: "80px", boxShadow:"none"}}
                    >
                      <h2
                        style={{
                          marginTop: "100px",
                          color: "#f50057",
                          background: "pink",
                          borderRadius: "5px 5px 0px 0px",
                        }}
                      >
                        Popular
                      </h2>
                      <CardActionArea
                        style={{ marginTop: "10px"}}
                      >
                      <Link to="/details" style={{textDecoration:"none"}}>
                      <CardMedia
                          component="img"
                          alt="cones"
                          height="sm"
                          style={{ border: "2px dash", marginBottom:"10px"}}
                          image="./public/ice.png"
                        />
                        <CardContent style={{background:"pink"}}>
                          <Typography gutterBottom variant="h5" component="div" color="#f50057" fontWeight="bold">
                            Strawberry Ice
                          </Typography>
                          <Typography gutterBottom variant="h6" component="div">
                            <Rating name="simple-controlled" value={3} style={{color:"#ff9100"}}/>
                          </Typography>
                          <button style={{borderRadius:"50px", color:"white", border:"none", background:"#f50057", width:"45px", height:"45px", fontSize:"28px", marginTop:"20px"}}>+</button>
                        </CardContent>
                      </Link>
                        
                      </CardActionArea>
                    </Card>
                  </Grid>
                </React.Fragment>
              ))}
            </List>
          </Paper>
        </Box>
      </Container>

      <AppBar position="fixed" color="primary" sx={{ top: "auto", bottom: 0 }}>
        <Toolbar style={{ background: "white" }}>
          <IconButton style={{ color: "#f50057" }} aria-label="open drawer">
            <DeckIcon />
          </IconButton>
          <Box sx={{ flexGrow: 1 }} />
        </Toolbar>
      </AppBar>
    </React.Fragment>
  );
}
