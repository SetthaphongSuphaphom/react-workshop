import './App.css'
import ButtomNav from './components/ButtomNav'
import Homepage from './components/Homepage'
import { Routes, Route, Navigate } from "react-router-dom";
import Products from './components/Products';

function App() {

  return (
    <>
      <ButtomNav/>
      <Routes>
        <Route path="/" element={<Homepage/>} />
        <Route path="/products" element={<Products />} />
        <Route path="/feed" element={<Navigate to="/" />}></Route>
      </Routes>
      
    </>
  )
}

export default App
