import React from "react";
import { Bar } from "react-chartjs-2";
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  BarElement,
  Tooltip,
} from "chart.js";
import CssBaseline from "@mui/material/CssBaseline";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";

ChartJS.register(CategoryScale, LinearScale, BarElement, Tooltip);

export default function BarChart() {
  const data = {
    labels: ["S", "M", "T", "W", "T", "F", "S"],
    datasets: [
      {
        data: [12, 8, 10, 4, 3, 2, 3],
        backgroundColor: [
          "#2b72f7",
          "#2b72f7",
          "#2b72f7",
          "#2b72f7",
          "#e7f1fc",
          "#e7f1fc",
          "#e7f1fc",
        ],
        borderWidth: 0,
        barThickness: 13,
        borderRadius:10,
      },
    ],
  };

  const options = {
    maintainAspectRatio: false,
    responsive: true,
    scales: {
      y: {
        beginAtZero: true,
        max: 20,
        display:false,
      },
      x:{
        grid: {
          display: false,
        },
      }
    },
    plugins: {
      legend: {
        display: false,
      },
      tooltip: {
        enabled: false,
      },
    },
  };

  return (
    <React.Fragment>
      <CssBaseline />
      <Container>
        <Box display={'flex'} maxWidth={"100%"}>
          <Bar data={data} options={options} />
        </Box>
      </Container>
    </React.Fragment>
  );
}
