import * as React from "react";
import "./Homepage.css";
import CssBaseline from "@mui/material/CssBaseline";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import Typography from "@mui/material/Typography";
import { useTheme } from "@mui/material/styles";
import Stack from "@mui/material/Stack";
import SushiLogo from "../assets/SushiLogo.svg";
import { colors } from "@mui/material";

export default function Homepage() {
  const theme = useTheme();

  return (
    <React.Fragment>
      <CssBaseline />
      <Container maxWidth="100%">
        <Box sx={{ width: "100%" }}>
          <Stack>
            <Box
              display={"flex"}
              justifyContent={"space-between"}
              sx={{ borderTop: "1px solid #ddd", p: 2 }}
            >
              <Box display={"inline-flex"}>
                <Box sx={{ borderRadius: 5, overflow: "hidden", mr: 2 }}>
                  <img src={SushiLogo} alt="Sake Sushi" width={"50px"} />
                </Box>
                <Box>
                  <Typography sx={{ fontWeight: "bold", fontSize: "1.2rem" }}>
                    Sake Sushi Bar
                  </Typography>
                  <Typography sx={{ fontSize: "0.8rem" }}>
                    Food & Drink
                  </Typography>
                </Box>
              </Box>

              <Box>
                <Typography sx={{ fontWeight: "bold", fontSize: "1.2rem", color:'#2b72f7' }}>
                  -37 $
                </Typography>
                <Typography sx={{ textAlign:'center',fontSize: "0.8rem", color:'#aaa' }}>Spent</Typography>
              </Box>
            </Box>
            <Box
              display={"flex"}
              justifyContent={"space-between"}
              sx={{ borderTop: "1px solid #ddd", p: 2 }}
            >
              <Box display={"inline-flex"}>
                <Box sx={{ borderRadius: 5, overflow: "hidden", mr: 2 }}>
                  <img src={SushiLogo} alt="Sake Sushi" width={"50px"} />
                </Box>
                <Box>
                  <Typography sx={{ fontWeight: "bold", fontSize: "1.2rem" }}>
                    Sake Sushi Bar
                  </Typography>
                  <Typography sx={{ fontSize: "0.8rem" }}>
                    Food & Drink
                  </Typography>
                </Box>
              </Box>

              <Box>
                <Typography sx={{ fontWeight: "bold", fontSize: "1.2rem", color:'#33e36b' }}>
                  +37 $
                </Typography>
                <Typography sx={{ textAlign:'center',fontSize: "0.8rem", color:'#aaa' }}>Left</Typography>
              </Box>
            </Box>
            <Box
              display={"flex"}
              justifyContent={"space-between"}
              sx={{ borderTop: "1px solid #ddd", p: 2 }}
            >
              <Box display={"inline-flex"}>
                <Box sx={{ borderRadius: 5, overflow: "hidden", mr: 2 }}>
                  <img src={SushiLogo} alt="Sake Sushi" width={"50px"} />
                </Box>
                <Box>
                  <Typography sx={{ fontWeight: "bold", fontSize: "1.2rem" }}>
                    Sake Sushi Bar
                  </Typography>
                  <Typography sx={{ fontSize: "0.8rem" }}>
                    Food & Drink
                  </Typography>
                </Box>
              </Box>

              <Box>
                <Typography sx={{ fontWeight: "bold", fontSize: "1.2rem", color:'#2b72f7' }}>
                  -37 $
                </Typography>
                <Typography sx={{ textAlign:'center',fontSize: "0.8rem", color:'#aaa' }}>Spent</Typography>
              </Box>
            </Box>
          </Stack>
        </Box>
      </Container>
    </React.Fragment>
  );
}
