import * as React from "react";
import CssBaseline from "@mui/material/CssBaseline";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import Typography from "@mui/material/Typography";
import { useTheme } from "@mui/material/styles";
import { Pie } from "react-chartjs-2";
import { Chart as ChartJS, ArcElement, Tooltip, Legend } from "chart.js";

ChartJS.register(ArcElement, Tooltip, Legend);

export default function Loans() {
  const data = {
    datasets: [
      {
        data: [320, 500],
        backgroundColor: ["#b1cefe", "#2675f4"],
      },
    ],
  };
  const options = {
    maintainAspectRatio: false,
    responsive: true,
    plugins: {
      legend: {
        display: false,
      },
      tooltip: {
        enabled: false,
      },
    },
    elements: {
      arc: {
        borderWidth: 0,
      },
    },
  };

  return (
    <React.Fragment>
      <CssBaseline />
      <Container maxWidth="100%">
        <Box display={'flex'} flexDirection={'column'} overflow={'hidden'} >
          <Box sx={{ mt: 3 }}>
            <Typography fontWeight={"bold"} fontSize={"1.5rem"}>
              Loans
            </Typography>
          </Box>
          <Box
            display={"flex"}
            sx={{
              background: "#edf4fe",
              alignItems: "center",
              borderRadius: 5,
              p: 2,
              justifyContent: "center",
            }}
          >
            <Box width={"35%"} sx={{mr:2}}>
              <Pie options={options} data={data} />
            </Box>
            <Box sx={{ ml: 2 }}>
              <Box>
                <Typography sx={{ color: "#aaa" }}>Student Loan</Typography>
                <Typography sx={{ fontWeight: "bold", fontSize: "1.5rem" }}>
                  3,280$
                </Typography>
              </Box>
              <Box>
                <Typography sx={{ color: "#aaa" }}>
                  Payment date 29/8
                </Typography>
                <Typography sx={{ fontWeight: "bold", fontSize: "1.2rem" }}>
                  Amount 260$
                </Typography>
              </Box>
            </Box>
          </Box>
          <Box
            display={"flex"}
            sx={{
              width:'80%',
              my: 3,
              mx:'auto',
              height: "50px",
              border: "dashed 1px #aaa",
              alignItems: "center",
              borderRadius: 3,
              p: 2,
              px:10,
              justifyContent: "center",
            }}
          >
            <Typography sx={{ color: "#aaa", fontSize:'0.9rem'}}>Request Loan</Typography>
          </Box>
        </Box>
        <Box sx={{ mt: 2 }}>
            <Typography fontWeight={"bold"} fontSize={"1.5rem"}>
              Cashback
            </Typography>
          </Box>
      </Container>
    </React.Fragment>
  );
}
