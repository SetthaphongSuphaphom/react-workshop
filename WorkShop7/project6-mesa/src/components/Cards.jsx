import * as React from "react";
import CssBaseline from "@mui/material/CssBaseline";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import Container from "@mui/material/Container";
import Typography from "@mui/material/Typography";
import mastercardicon from "../assets/mastercardicon.svg";
import visaicon from "../assets/visaicon.svg";
import AddIcon from '@mui/icons-material/Add';
import "./Cards.css"

export default function Cards() {

  return (
    <React.Fragment>
      <CssBaseline />
      <Container maxWidth="100%">
        
        <Box>
          <Grid container spacing={2}>
          <Grid item xs={6} sm={6} md={6}>
              <Box
                className="box"
                sx={{
                  background: "#feefe8",
                }}
              >
                <Box>
                  <Typography
                    sx={{ fontWeight: "bold", fontSize: "1.2rem", mb: 2 }}
                  >
                    1223 *** 5217
                  </Typography>
                </Box>
                <Box display={"inline-flex"} justifyContent={"space-between"} alignItems={'center'}>
                  <Box>
                    <Typography sx={{ color: "#aaa", fontSize:'0.8rem' }}>Exp Date</Typography>
                    <Typography sx={{ fontWeight: "bold", fontSize: "1rem" }}>
                      12/23
                    </Typography>
                  </Box>
                  <Box>
                    <img src={mastercardicon} alt="mastercard" width={'35px'}  />
                  </Box>
                </Box>
              </Box>
            </Grid>
            

            <Grid item xs={6} sm={6} md={6}>
              <Box
                className="box"
                sx={{
                  background: "#e6eef8",
                }}
              >
                <Box>
                  <Typography
                    sx={{ fontWeight: "bold", fontSize: "1.2rem", mb: 2 }}
                  >
                    1223 *** 5217
                  </Typography>
                </Box>
                <Box display={"inline-flex"} justifyContent={"space-between"} alignItems={'center'}>
                  <Box>
                    <Typography sx={{ color: "#aaa", fontSize:'0.8rem' }}>Exp Date</Typography>
                    <Typography sx={{ fontWeight: "bold", fontSize: "1rem" }}>
                      12/23
                    </Typography>
                  </Box>
                  <Box>
                    <img src={visaicon} alt="visa" width={'40px'}/>
                  </Box>
                </Box>
              </Box>
            </Grid>

            <Grid item xs={6} sm={6} md={6}>
              <Box
                className="box"
                sx={{
                  background: "#fbdce2",
                }}
              >
                <Box>
                  <Typography
                    sx={{ fontWeight: "bold", fontSize: "1.2rem", mb: 2 }}
                  >
                    1223 *** 5217
                  </Typography>
                </Box>
                <Box display={"inline-flex"} justifyContent={"space-between"} alignItems={'center'}>
                  <Box>
                    <Typography sx={{ color: "#aaa", fontSize:'0.8rem' }}>Exp Date</Typography>
                    <Typography sx={{ fontWeight: "bold", fontSize: "1rem" }}>
                      12/23
                    </Typography>
                  </Box>
                  <Box>
                    <img src={mastercardicon} alt="mastercard" width={'35px'}  />
                  </Box>
                </Box>
              </Box>
            </Grid>

            <Grid item xs={6} sm={6} md={6}>
              <Box
                maxWidth={"80%"}
                display={"flex"}
                sx={{
                  border:'0.5px dashed #aaa',
                  borderRadius: 3,
                  p: 1.5,
                  flexDirection: "row",
                  alignItems:'center',
                  justifyContent:'center',
                  mt:2.5,
                  mx:'auto',
                  height:'80px',
                  overflow:'hidden'
                }}
              >
                <Box sx={{mx:1.5}}>
                  <AddIcon/>
                </Box>
                <Box>
                <Typography width={'50px'} fontSize={'0.7rem'}>Request Card</Typography>
                </Box>
              </Box>
            </Grid>
          </Grid>
        </Box>
      </Container>
    </React.Fragment>
  );
}
