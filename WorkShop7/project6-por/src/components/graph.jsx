import {Line} from 'react-chartjs-2'
import Button from '@mui/material/Button';

import {
    Chart as ChartJS,
    LineElement,
    CategoryScale,
    LinearScale,
    PointElement,
    
} from 'chart.js';
import { border, margin } from '@mui/system';

ChartJS.register(
    LineElement,
    CategoryScale,
    LinearScale,
    PointElement,
    
)

function LineChart(){
    const data= {
        labels : ['Phy','che','bio','Eng'],
        datasets : [{
            labels: 'Sales of the week',
            data:[1,4,2,3],
            backgroundColor:'white',
            pointBorderColor:'white',
            // fill :true,
            tension: 0.4,
            pointBorderColor:'white',
            yAxisTitle:"0.6",
            color:'white'
            

        }]
    }
    const option ={
        // plugins : {
        //     legend: true
        // },
        scales:{
            y:{
                // min:3,
                // max: 6            
            }
        }
    }
    return(
        <div>
            <div className='line-chart' style={
                {
                    borderColor:'white',
                    stroke:'white'

                }
            }>
            
            <Line className='line-chart'  
            data= {data}
            option= {option}
            ></Line>
            </div>
            <div className='Box-button' sx={{marginLeft:'40'}}>
            <Button className='button' sx={{color:'white',borderColor:'white',marginRight:2.7,height:25,marginTop:3}} variant="outlined">D</Button>
            <Button className='button' sx={{color:'white',borderColor:'#00d3da',marginRight:2.7,height:25,marginTop:3, backgroundColor:'#00d3da'}} variant="outlined">Weekly</Button>
            <Button className='button' sx={{color:'white',borderColor:'white',marginRight:2.5,height:25,marginTop:3}} variant="outlined">M</Button>
            <Button className='button' sx={{color:'white',borderColor:'white',height:25,marginTop:3}} variant="outlined">Y</Button>
            </div>
        </div>
    );
}
export default LineChart;