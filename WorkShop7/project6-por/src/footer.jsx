import Link from "@mui/material/Link"
import { BiHomeSmile,BiNotepad } from "react-icons/bi";
import { BsFillBarChartLineFill } from "react-icons/bs";
import { AiFillBell } from "react-icons/ai";
import { FaRegUser } from "react-icons/fa";
import './footer.css'




export default function Footer(){
    return(
        <div className="containerfooter">
            <Link className="Iconfooter" ><BiHomeSmile/></Link>
            <Link ><BsFillBarChartLineFill/></Link>
            <Link ><AiFillBell/></Link>
            <Link ><BiNotepad/></Link>
            <Link ><FaRegUser/></Link>
        </div>
    )
}