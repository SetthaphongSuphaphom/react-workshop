import Logo from "../Image/eiei.jpg";
import "./Header.css";

function Header(props) {
  return (
    <nav>
      <img src={Logo} alt="logo" className="logo" />
      <a href="/">{props.title}</a>
    </nav>
  );
}
export default Header;
