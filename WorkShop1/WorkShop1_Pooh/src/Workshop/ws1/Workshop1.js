import eiei from "./eiei.jpg";
import './ws1.css';

function workshop1() {
  return (
    <div className="content1">
      <br />
      <h1 style={{ textAlign: "center" }}> Workshop 1</h1>

      <img
        src={eiei}
        style={{ display: "block", marginTop: "20px", borderRadius: "300px" }}
        className="App-logo"
        alt="eiei"
      />

      
      <h1 style={{ textAlign: "center"}}> Suphasan Saejong</h1>
      <div style={{margin:"20px"}}>
      <h2> Nickname : Pooh</h2>
      <h2> Position : Front-end</h2>
      <h2>
        Skill : <br />
        <ul>
          <li>html</li>
          <li>css</li>
          <li>Javascript</li>
          <li>python</li>
          <li>Java</li>
          <li>C</li>
        </ul>
      </h2>
      <h2>
        Hobby : <br />
        <ul>
          <li>movie</li>
          <li>game</li>
        </ul>
      </h2>
      </div>
      <br />
    </div>
  );
}

export default workshop1;
