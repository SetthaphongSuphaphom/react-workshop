import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'
import Calculate from './workshop2/Cal.jsx'


function App() {
  return (
    <>
      <Calculate/>
    </>
  )
}

export default App
