import "./style.css";
import nongmesa from "./nongmesa.jpg";

function Portfo() {
  return (
    <div className="container">
      <div className="head">
        <img src={nongmesa} alt="profile picture" />
        <h2>โสภิดา ทิพยะวัฒน์</h2>
      </div>
      <div className="content">
        <p>
          <span>ชื่อเล่น :</span> เมษา
        </p>
        <p>
          <span>ตำแหน่ง :</span> Frontend Developer
        </p>
        <p>
          <span>Skill : </span>
        </p>
        <ul>
          <li>HTML</li>
          <li>CSS (tailwindCSS, Boostrap)</li>
          <li>Javascript</li>
        </ul>
        <p>
          <span>งานอดิเรก : </span>
        </p>
        <ul>
          <li>เล่นเกม ฟังเพลง</li>
          <li>คุยกับต้นไม้</li>
          <li>นอน</li>
          <li>ทำความสะอาดห้อง</li>
        </ul>
      </div>
    </div>
  );
}

export default Portfo;
