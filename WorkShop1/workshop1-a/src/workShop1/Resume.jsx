import { useState } from 'react'
import reactLogo from '../assets/react.svg'
import viteLogo from '/vite.svg'
import '../css/Resume.css'
import chanyutIMG from '../assets/chanyutIMG.jpg'

function Resume() {

  return (
   
    <div className='My-header'>
      <div className='My-img'>
        <img src={chanyutIMG} alt="Chanyut" />
        <h1 className='name'>Chanyut Dongthana</h1>
      </div>
      <div className='my-data'>
        <div className="my-data-text">
          <div className="grid-top">
            <div>ชื่อเล่น : เอ</div>
            <p>ตำแหน่ง : Front-End</p>
            <p>skill : </p>
            <ul>
              <li>html : low</li>
              <li>css : low</li>
            </ul>
          </div>
          <div className="grid-bottom">
            <p>งานอดิเรก : </p>
            <ul>
              <li>ฟุตซอล</li>
              <li>ว่ายน้ำ</li>
              <li>ดนตรี</li>
              <li>เขียนโค้ด</li>
            </ul>
          </div>
        </div>
      </div>
  </div>
  )
}
export default Resume