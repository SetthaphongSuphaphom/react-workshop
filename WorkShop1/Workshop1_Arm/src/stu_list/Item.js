export default function Item(props) {
    const {data,delStu} = props;
  return (
    <>
      <li key={data.id}>
        <p>
          {data.id}-{data.name}
        </p>
        <button onClick={() => delStu(data.id)}>ลบ</button>
      </li>
    </>
  );
}
