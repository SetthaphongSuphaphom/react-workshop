import Profile from '../image/profile.jpg';

import '../styles.css';

function Resu() {
  return (
    <div
      style={{
        position: "absolute",
        top: "50%",
        left: "50%",
        transform: "translate(-50%, -50%)",
        marginTop: "150px",
        width: "70%",
        fontSize: "20px", 
      }}
    >
      <div 
        style={{ textAlign: "center" }}>
        <img
          src={Profile}
          alt="Profile"
          style={{ width: "220px", borderRadius: "100%", }}
        />
  
        <h1>ชื่อ: Puthaned Khuakum</h1>
      </div>
      <h2>Internship Front end developer</h2>
      <h2>ชื่อเล่น: </h2><li>แทน</li>
      <h2>ข้อมูลส่วนตัว</h2>
      <ul>
        <li>อายุ: 21 ปี</li>
        <li>เบอร์โทร: 065-747-0303</li>
        <li>อีเมล์: puthaned@kkumail.com</li>
      </ul>
      <h2>การศึกษา</h2>
      <ul>
        <li>
          <strong>ปริญญาตรี:</strong>  มหาวิทยาลัยขอนเเก่น -
          สาขาวิชาวิทยาการคอมพิวเตอร์
        </li>
      </ul>
      <h2>Skill</h2>
      <ul>
        <li>HTML</li>
        <li>CSS</li>
        <li>JAVASCRIPT</li>
        <li>PYTHON</li>
        
      </ul>
      <h2>งานอดิเรก</h2>
      <ul>
        <li>เล่นดนตรี</li>
        <li>เล่นกีฬาฟุตบอล</li>
      </ul>
    </div>
  );
}

export default Resu;
