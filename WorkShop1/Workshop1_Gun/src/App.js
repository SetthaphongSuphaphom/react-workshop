import Workshop1 from "./WS/WS1/Workshop1";
import Workshop2 from "./WS/WS2/Workshop2";
import Workshop3 from "./WS/WS3/Workshop3";
import Navbar from "./Navbar/Nav";
import "./App.css";
function App() {
  return (
    <>
      <Navbar />
      <Workshop2/>
    </>
  );
}

export default App;
