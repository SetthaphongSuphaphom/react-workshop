import { useState } from "react";
import "./Workshop2.css";
function Workshop2() {
  // asd
  const [count, setCount] = useState(0);
  const [students, setStudent] = useState([
    { id: 1, name: "wqe" },
    { id: 2, name: "asd" },
    { id: 3, name: "dfgd" },
    { id: 4, name: "jrth" },
    { id: 5, name: "ijokp" },
  ]);
  const [show, setShow] = useState(true);
  function deleteStudent(id) {
    setStudent(students.filter((item) => item.id !== id));
  }
  const [sum, setSum] = useState(0);
  const [num1, setNum1] = useState(0);
  const [num2, setNum2] = useState(0);
  const [ope, setOpe] = useState("");
  function calculate(num1, num2, ope) {
    console.log(num1, num2, ope);
    num1 = parseInt(num1);
    num2 = parseInt(num2);
    if (ope === "+") {
      setSum(num1 + num2);
    }
    if (ope === "-") {
      setSum(num1 - num2);
    }
    if (ope === "*") {
      setSum(num1 * num2);
    }
    if (ope === "/") {
      setSum(num1 / num2);
    }
  }

  return (
    <>
      <h1>{count}</h1>
      <button onClick={() => setCount(count + 1)}>เพิ่มค่า</button>
      <button onClick={() => setCount(count - 1)}>ลดค่า</button>
      <button onClick={() => setCount(0)}>reset</button>
      <br />
      <h1>จำนวนนักเรียน = {show && students.length}</h1>
      <button onClick={() => setShow(!show)}>{show ? "ซ่อน":"แสดง"}</button>
      <ul>
        {show &&
          students.map((item) => (
            <li key={item.id}>
              {item.id} - {item.name}
              <button onClick={() => deleteStudent(item.id)}>ลบ</button>
            </li>
          ))}
      </ul>
      <hr />
      <div className="calculater">
        <div>
          <input
            type="number"
            value={num1}
            onChange={(e) => setNum1(e.target.value)}
          />
          <select value={ope} onChange={(e) => setOpe(e.target.value)}>
            <option>select operater</option>
            <option value="+">+</option>
            <option value="-">-</option>
            <option value="*">*</option>
            <option value="/">/</option>
          </select>
          <input
            type="number"
            value={num2}
            onChange={(e) => setNum2(e.target.value)}
          />
          <span> = {sum}</span>
        </div>
        <div>
          <button onClick={() => calculate(num1, num2, ope)}>calcutate</button>
        </div>
      </div>
    </>
  );
}

export default Workshop2;
