import { useState } from "react";
import FormCalculate from "./components/FormCalculate.jsx";
import "./index.css";

function App() {
  const [count, setCount] = useState(0);

  return (
    <div>
      <FormCalculate></FormCalculate>
    </div>
  );
}

export default App;
