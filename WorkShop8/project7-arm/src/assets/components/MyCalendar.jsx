import { DemoContainer, DemoItem } from "@mui/x-date-pickers/internals/demo";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { DateCalendar } from "@mui/x-date-pickers/DateCalendar";
import "./MyCalendar.css";
import { Box, Button, ButtonGroup, styled } from "@mui/material";
import { useState } from "react";

const CustomButton = styled(Button)(({ isClicked }) => ({
  backgroundColor: isClicked ? "#404040" : "#ffffff",
  fontFamily: "Poppins, sans-serif",
  borderRadius: "20em",
  border: "10px",
  color: isClicked ? "#ffffff" : "#212121",
  width: isClicked ? "70%" : "30%",
  transition: "all 0.2s ease-in-out",
  "&:hover": {
    backgroundColor: isClicked ? "#353535" : "#fff",
  },
}));

export default function MyCalendar() {
  const [isClicked, setIsClicked] = useState(false);
  const [view, setView] = useState("day");
  const handleButtonClick = () => {
    setIsClicked(!isClicked);
    if (isClicked) {
      setView("day");
    } else {
      setView("month");
    }
  };

  return (
    <LocalizationProvider dateAdapter={AdapterDayjs}>
      <DemoContainer components={["DateCalendar", "DateCalendar"]}>
        <DemoItem>
          <Box>
            <ButtonGroup disableElevation variant="contained" fullWidth>
              <CustomButton
                isClicked={!isClicked}
                size={!isClicked ? "large" : "medium"}
                onClick={handleButtonClick}
                style={{ borderRadius: "20em", border: "0px" }}
              >
                Upcoming
              </CustomButton>
              <CustomButton
                isClicked={isClicked}
                size={isClicked ? "large" : "medium"}
                onClick={handleButtonClick}
                style={{ borderRadius: "20em", border: "0px" }}
              >
                All
              </CustomButton>
            </ButtonGroup>
          </Box>
          <DateCalendar
            showDaysOutsideCurrentMonth
            readOnly
            disableFuture={true}
            disablePast={true}
            view={view}
            dayOfWeekFormatter={(day)=>day}
            style={{
              display: "flex",
              width: "90%",
              alignSelf: "center",
              height: "15em",
            }}
          />
        </DemoItem>
      </DemoContainer>
    </LocalizationProvider>
  );
}
