import "./MyHeader.css";
import SettingsIcon from "@mui/icons-material/Settings";
import AddIcon from "@mui/icons-material/Add";

export default function MyHeader() {
  return (
    <div className="header">
      <SettingsIcon/>
      <p>Payments</p>
      <AddIcon/>
    </div>
  );
}
