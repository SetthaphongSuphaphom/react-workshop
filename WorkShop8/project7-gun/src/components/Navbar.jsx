import {
    BottomNavigation,
    BottomNavigationAction,
    Box,
    createTheme,
  } from "@mui/material";
  import HomeIcon from "@mui/icons-material/Home";
  import CalendarMonthIcon from "@mui/icons-material/CalendarMonth";
  import DataUsageIcon from "@mui/icons-material/DataUsage";
  import SettingsIcon from "@mui/icons-material/Settings";
  import { useState } from "react";
  import { grey } from "@mui/material/colors";
  import { ThemeProvider } from "@emotion/react";
  export default function Navbar() {
    const [value, setValue] = useState("recents");
  
    const handleChange = (event, newValue) => {
      setValue(newValue);
    };
    const theme = createTheme({
      palette: {
        primary: {
          main: grey[900],
        },
      },
    });
    return (
      <ThemeProvider theme={theme}>
        <BottomNavigation
          sx={{
            width: "98vw",
            height: "9vh",
            position: "fixed",
            bottom: 5,
            left: "50%",
            backgroundColor: "#212121",
            borderRadius: "30px",
            display: "flex",
            alignItems: "center",
            overflow: "hidden",
            transform: "translate(-50%, 0%)",minWidth:"300px"
          }}
          value={value}
          onChange={handleChange}
        >
          <Box
            sx={{
              width: "50px",
              height: "50px",
              borderRadius: "50%",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              overflow: "hidden",
              m: 2,
            }}
          >
            <BottomNavigationAction
              sx={{ width: "100%", height: "100%", color: "#fff"}}
              value="home"
              icon={<HomeIcon />}
            />
          </Box>
          <Box
            sx={{
              width: "50px",
              height: "50px",
              borderRadius: "50%",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              overflow: "hidden",
              m: 2,
            }}
          >
            <BottomNavigationAction
              sx={{ width: "100%", height: "100%", color: "#fff" }}
              value="calendar"
              icon={<CalendarMonthIcon />}
            />
          </Box>
          <Box
            sx={{
              width: "50px",
              height: "50px",
  
              borderRadius: "50%",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              overflow: "hidden",
              m: 2,
            }}
          >
            <BottomNavigationAction
              sx={{ width: "100%", height: "100%", color: "#fff" }}
              value="data"
              icon={<DataUsageIcon />}
            />
          </Box>
          <Box
            sx={{
              width: "50px",
              height: "50px",
  
              borderRadius: "50%",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              overflow: "hidden",
              m: 2,
            }}
          >
            <BottomNavigationAction
              sx={{ width: "100%", height: "100%", color: "#fff" }}
              value="setting"
              icon={<SettingsIcon />}
            />
          </Box>
        </BottomNavigation>
      </ThemeProvider>
    );
  }