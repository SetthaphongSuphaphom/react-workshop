import { Box, Typography } from "@mui/material";
import Graph2 from "./Graph2";

export default function Graph() {
  return (
    <Box
      sx={{
        width: "100%",
        backgroundColor: "#212121",
        color: "#fff",
        borderRadius: "25px",
        maxWidth:"700px",
        minWidth:"300px"
      }}
    >
      <Box sx={{ display: "flex", justifyContent: "space-between" }}>
        <Typography sx={{ padding: "12px" }}>Spending</Typography>
        <Typography sx={{ padding: "12px" }}>This week</Typography>
      </Box>
      <Box>
        <Typography
          sx={{ padding: "0 12px", fontSize: "30px", fontWeight: "900" }}
        >
          $456.78
        </Typography>
      </Box>
      <Box
        sx={{
          padding: "10px",
          bgcolor: "#fff",
          width: "30%",
          color: "#000",
          margin: "0 12px",
          borderRadius: "25px",
          textAlign: "center",
        }}
      >
        <Typography sx={{ fontSize: "12px" }}>$32 below</Typography>
        <Typography sx={{ fontSize: "12px" }}>last week</Typography>
      </Box>

      <Graph2 />
    </Box>
  );
}
