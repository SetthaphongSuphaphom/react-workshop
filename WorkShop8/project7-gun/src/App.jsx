import * as React from "react";
import CssBaseline from "@mui/material/CssBaseline";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import Header from "./components/Header";
import Navbar from "./components/Navbar";
import Graph from "./components/Graph";
import Content from "./components/Contents";
import Graph2 from "./components/Graph2";
import { Grid } from "@mui/material";
function App() {
  return (
    <React.Fragment>
      <CssBaseline />
      <Container maxWidth="sx" className="container">
        <Box sx={{ height: "100vh" }}>
          <Header />
          <Grid container spacing={2}>
            <Grid item xs={12} xl={6}>
              <Graph />
            </Grid>
            <Grid item xs={12} xl={6}>
              <Content />
            </Grid>
          </Grid>
          
          
          <Navbar />
        </Box>
      </Container>
    </React.Fragment>
  );
}

export default App;
