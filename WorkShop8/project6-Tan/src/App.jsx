import './App.css';
import Home from "./component/Home.jsx";
import { Routes, Route, BrowserRouter } from "react-router-dom";
import MyCourses from "./component/MyCourses.jsx";


function App() {
 

  return (
    <BrowserRouter>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/my" element={<MyCourses />} />
        </Routes>
      </BrowserRouter>
  
      
    
  );
}

export default App;

