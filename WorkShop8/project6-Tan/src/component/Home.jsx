import React, { useState } from "react";
import "./Home.css";
import { styled, alpha } from "@mui/material/styles";
import Box from "@mui/material/Box";
import InputBase from "@mui/material/InputBase";
import SearchIcon from "@mui/icons-material/Search";
import Container from "@mui/material/Container";
import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import { Avatar, Button, CardActionArea, IconButton } from "@mui/material";
import CssBaseline from "@mui/material/CssBaseline";
import BottomNavigation from "@mui/material/BottomNavigation";
import BottomNavigationAction from "@mui/material/BottomNavigationAction";
import RestoreIcon from "@mui/icons-material/Restore";
import FavoriteIcon from "@mui/icons-material/Favorite";
import ArchiveIcon from "@mui/icons-material/Archive";
import Paper from "@mui/material/Paper";
import TuneIcon from "@mui/icons-material/Tune";
import WaterDropIcon from "@mui/icons-material/WaterDrop";
import BoltIcon from "@mui/icons-material/Bolt";
import StarOutlineIcon from "@mui/icons-material/StarOutline";
import BookmarkBorderIcon from "@mui/icons-material/BookmarkBorder";
import TopicIcon from "@mui/icons-material/Topic";
import AccessTimeIcon from "@mui/icons-material/AccessTime";

import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import HomeIcon from '@mui/icons-material/Home';
import MenuBookIcon from '@mui/icons-material/MenuBook';
import NotificationAddIcon from '@mui/icons-material/NotificationAdd';
import PermIdentityIcon from '@mui/icons-material/PermIdentity';
import { Link } from "react-router-dom";

const Search = styled("div")(({ theme }) => ({
  position: "relative",
  borderRadius: theme.shape.borderRadius,
  backgroundColor: alpha(theme.palette.common.white, 0.15),
  "&:hover": {
    backgroundColor: alpha(theme.palette.common.white, 0.25),
  },
  marginLeft: 0,
  width: "100%",
  [theme.breakpoints.up("sm")]: {
    marginLeft: theme.spacing(1),
    width: "auto",
  },
}));
const SearchIconWrapper = styled("div")(({ theme }) => ({
  padding: theme.spacing(0, 20),
  height: "100%",
  width: "50%",
  position: "absolute",
  pointerEvents: "none",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: "inherit",
  "& .MuiInputBase-input": {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)})`,
    transition: theme.transitions.create("width"),
    width: "100%",
    [theme.breakpoints.up("sm")]: {
      width: "12ch",
      "&:focus": {
        width: "20ch",
      },
    },
  },
}));

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === "dark" ? "#1A2027" : "#fff",
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: "center",
  color: theme.palette.text.secondary,
}));

function MyChips() {
  const [selected, setSelected] = useState("All Topic");

  const handleChipClick = (value) => {
    setSelected(value);
  };

  const handleChipKeyDown = (event, value) => {
    if (event.key === "Enter") {
      setSelected(value);
    }
  };
}

export default function Home() {
  const [alignment, setAlignment] = React.useState("web");

  const handleChange = (event, newAlignment) => {
    setAlignment(newAlignment);
  };

  const responsive = {
    superLargeDesktop: {
      // the naming can be any, depends on you.
      breakpoint: { max: 4000, min: 3000 },
      items: 5
    },
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 3
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 2
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 1
    }
  };




  return (
    <Grid>
      <div className="container">
        <React.Fragment>
          <CssBaseline />

          <Container maxWidth="lg" minWidth="380">
            <Box sx={{ bgcolor: "#cfe8fc", height: "110vh" }}>
              <Box display="flex" alignItems="center">
                <Box className="head" sx={{ flexGrow: 2 }}>
                  <Typography color="#696969">Hollo</Typography>
                  <Typography variant="h3">Alonzo Lee</Typography>
                </Box>
                <Avatar
                  img
                  src="https://img.freepik.com/premium-vector/charming-young-woman-business-attire-blue-background_481311-9.jpg?w=740"
                ></Avatar>
              </Box>

              <Box sx={{ flexGrow: 2 }}>
                <Box display="flex">
                  <Box className="search" sx={{ m: 1 }}>
                    {" "}
                    <Search>
                      <SearchIconWrapper>
                        <SearchIcon />
                      </SearchIconWrapper>
                      <StyledInputBase
                        placeholder="Search…"
                        inputProps={{ "aria-label": "search" }}
                      />
                    </Search>
                  </Box>

                  <Box
                    display="flex"
                    align-items="center"
                    sx={{
                      my: 1,
                      mr: 1,
                      p: 0.5,
                      bgcolor: "#1E90FF",
                      borderRadius: "20%",
                    }}
                  >
                    <IconButton variant="contained">
                      <TuneIcon sx={{ color: "#fff" }} />
                    </IconButton>
                  </Box>
                </Box>
              </Box>
              <h2>Courses</h2>

              <Box className="Courses">
                <Grid container alignItems="center" item xs={12}>
                  <Box sx={{ width: "100%", m: 1 }}>
                    <Grid
                      container
                      rowSpacing={1}
                      columnSpacing={{ xs: 2, sm: 2, md: 3 }}
                    >
                      <Grid item xs={6}>
                        <Item value="All Topic" sx={{ borderRadius: "20px" }}>
                          <Box display="flex" alignItems="center">
                            <Box display="flex">
                              <Box
                                sx={{
                                  p: 0.5,
                                  bgcolor: "#fff",
                                  borderRadius: "50%",
                                }}
                              >
                                
                                <IconButton color="primary">
                                  <WaterDropIcon />
                                </IconButton>
                              </Box>
                            </Box>
                              
                            <Box sx={{ ml: 1 }}>
                              <Typography>All Topic</Typography>
                            </Box>
                          </Box>
                        </Item>
                      </Grid>

                      <Grid item xs={6}>
                        <Item sx={{ borderRadius: "20px" }}>
                          <Box display="flex" alignItems="center">
                            <Box display="flex">
                              <Box
                                sx={{
                                  p: 0.5,
                                  bgcolor: "#FFD700",
                                  borderRadius: "50%",
                                }}
                              >
                                <IconButton>
                                  <BoltIcon sx={{ color: "#ffff" }} />
                                </IconButton>
                              </Box>
                            </Box>
                            <Box sx={{ ml: 1 }}>
                              <Typography>Poppular</Typography>
                            </Box>
                          </Box>
                        </Item>
                      </Grid>

                      <Grid item xs={6}>
                        <Item sx={{ borderRadius: "20px" }}>
                          <Box display="flex" alignItems="center">
                            <Box display="flex">
                              <Box
                                sx={{
                                  p: 0.5,
                                  bgcolor: "#6A5ACD",
                                  borderRadius: "50%",
                                }}
                              >
                                <IconButton>
                                  <StarOutlineIcon sx={{ color: "#ffff" }} />
                                </IconButton>
                              </Box>
                            </Box>
                            <Box sx={{ ml: 1 }}>
                              <Typography>Newest</Typography>
                            </Box>
                          </Box>
                        </Item>
                      </Grid>
                      <Grid item xs={6}>
                        <Item sx={{ borderRadius: "20px" }}>
                          <Box display="flex" alignItems="center">
                            <Box display="flex">
                              <Box
                                sx={{
                                  p: 0.5,
                                  bgcolor: "#008000",
                                  borderRadius: "50%",
                                }}
                              >
                                <IconButton>
                                  <BookmarkBorderIcon sx={{ color: "#ffff" }} />
                                </IconButton>
                              </Box>
                            </Box>
                            <Box sx={{ ml: 1 }}>
                              <Typography>Advance</Typography>
                            </Box>
                          </Box>
                        </Item>
                      </Grid>
                    </Grid>
                  </Box>
                </Grid>
              </Box>
              <br></br>
              <br></br>

              <React.Fragment>
                
                  <div className="card">
                  <Container maxWidth="100%"  sx={{ p: 1  }} display="flex" >
                    <Carousel responsive={responsive}>
                    <div className="item1">
                    
                    <Grid item xs={12} sm={6} md={4}  >
                      <Card sx={{ height:450,maxWidth: 345, minHeight: 60, m: 1, p: 5,borderRadius:"10%" , backgroundColor:"#87CEFA" }}    >
                        <CardContent>
                          <img src="https://img.freepik.com/free-vector/freelancer-working-laptop-her-house_1150-35054.jpg?w=740&t=st=1683611929~exp=1683612529~hmac=d20a31f2c85eef8714f53cab18429262f5ba408e39b36df8871ebccc0a4d84d6"></img>
                          <Typography gutterBottom variant="h5" component="div" style={{ marginTop: '10px' }} >
                            Digital Design Thinking
                          </Typography>
                          <Typography variant="body2" color="text.secondary">
                            <p>
                              Lizards are a widespread group of squamate
                              reptiles, with over ranging across all continents
                              except Antarctica{" "}
                            </p>
                            <IconButton color="primary">
                              <TopicIcon />
                            </IconButton>
                            17: Filed
                            <IconButton color="primary" sx={{ m: 1 }}>
                              <AccessTimeIcon />
                            </IconButton>
                            40 : Min
                          </Typography>
                        </CardContent>
                      </Card>
                    </Grid>
                    
                
                    </div>
                    <div className="item2">
                    <Grid item xs={12} sm={6} md={4}  >
                      <Card sx={{  height:450 ,maxWidth: 345, minHeight: 60, m: 1, p: 5,borderRadius:"10%", backgroundColor:"#DAA520" }}>
                        <CardContent>
                          <img   src="https://img.freepik.com/premium-vector/vector-concept-illustration-programmer-engineer-with-laptop-sitting-office-desk-holding-pen-while-coding-developing-flat-cartoon-style_270158-379.jpg?w=1060"></img>
                          <Typography gutterBottom variant="h5" component="div" style={{ marginTop: '20px' }} >
                           Web Development
                          </Typography>
                          <Typography variant="body2" color="text.secondary" >
                            <p>
                              Lizards are a widespread group of squamate
                              reptiles, with over ranging across all continents
                              except Antarctica{" "}
                            </p>
                            <br>
                            </br>
                            <IconButton color="primary">
                              <TopicIcon />
                            </IconButton>
                            17: Filed
                            <IconButton color="primary" sx={{ m: 1 }}>
                              <AccessTimeIcon />
                            </IconButton>
                            40 : Min
                          </Typography>
                        </CardContent>
                      </Card>
                    </Grid>           
                    </div>
                    <div className="item3">
                    <Grid item xs={12} sm={6}  md={4}  >
                      <Card sx={{ height:450,maxWidth: 345, minHeight: 60, m: 1, p: 5.5 ,borderRadius:"10%" , backgroundColor:"#6A5ACD"}}>
                        <CardContent>
                          <img  src="https://img.freepik.com/free-vector/computer-programming-camp-abstract-concept-illustration_335657-3921.jpg?w=740&t=st=1683687566~exp=1683688166~hmac=d6c1071c5b6bc173863b90ad9ddb0cc27525553b94d342e2847adf74ecdc4ee4"></img>
                          <Typography gutterBottom variant="h5" component="div" style={{ marginTop: '10px' }} >
                             Design Thinking
                          </Typography>
                          <Typography variant="body2" color="text.secondary">
                            <p>
                              Lizards are a widespread group of squamate
                              reptiles, with over ranging across all continents
                              except Antarctica{" "}
                            </p>
                            <br></br>
                            <IconButton color="primary">
                              <TopicIcon />
                            </IconButton>
                            17: Filed
                            <IconButton color="primary" sx={{ m: 1 }}>
                              <AccessTimeIcon />
                            </IconButton>
                            40 : Min
                          </Typography>
                        </CardContent>
                      </Card>
                    </Grid>    
                    </div>
                    </Carousel>
                  </Container>
                </div>           
                
              </React.Fragment>

            </Box>
          </Container>

          <Box>
            <Paper
              sx={{ position: "fixed", bottom: 0, left: 0, right: 30 }}
              elevation={2}
            >
              <BottomNavigation>
                <BottomNavigationAction 
                label="Home" 
                icon={<HomeIcon />} />
                <BottomNavigationAction
                  label="star"
                  icon={<StarOutlineIcon />}
                />

                <Link href="my">
                  <BottomNavigationAction
                  label="menu"
                  icon={<MenuBookIcon />}
                  />
                </Link>

              

          
                    
                
                <BottomNavigationAction
                  label="Notifi"
                  icon={<NotificationAddIcon />}
                />
                <BottomNavigationAction
                  label="person"
                  icon={<PermIdentityIcon />}
                />
              </BottomNavigation>

            </Paper>
          </Box>
        </React.Fragment>
      </div>
    </Grid>
  );
}
