import { useState } from "react";
import Box from "@mui/material/Box";
import "./App.css";
import Button from "@mui/material/Button";
import Fab from "@mui/material/Fab";
import Typography from "@mui/material/Typography";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import DataList from "./Components/DataList";
import GraphData from "./Components/Graph";
import Link from '@mui/material/Link';


function App() {
  return (
    <div className="out">

    <div className="container">
      <div className="Header">
        <Fab
          color="secondary"
          sx={{ backgroundColor: "#E4E3E4" }}
          aria-label="add"
        >
          <ArrowBackIcon color="inherit" style={{ color: "black" }} />
        </Fab>
        <Link href="#" sx={{textDecoration:"none",color:"black"}}>
        <Typography sx={{ fontWeight: "bold", fontSize: "2.4vh", marginRight:"3%" }}>
          Edit
        </Typography>
        </Link>
      </div>
      <div className="content">
        <GraphData />
        <DataList />
      </div>
    </div>
    </div>
  );
}


export default App;
