import about from "../images/about-us-page.png";
export default function About() {
  return (
    <div className="container">
      <h1 className="title">เกี่ยวกับเรา</h1>
      <img src={about} alt="about" />
    </div>
  );
}
