import Navbar from "./component/Navbar";
import { BrowserRouter as Router, Routes, Route ,Navigate} from "react-router-dom";

//contents
import Home from "./contents/Home";
import About from "./contents/About";
import Blogs from "./contents/Blogs";
import Details from "./contents/Details";
import NotFound from "./contents/NotFound";

function App() {
  return (
    <Router>
      <div className="App">
        <Navbar />
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/about" element={<About />} />
          <Route path="/blogs" element={<Blogs />} />
          <Route path="*" element={<NotFound />} />
          <Route path = "/home" element = {<Navigate to="/"/>}/>
          <Route path="/blog/:id" element={<Details />}></Route>
           
        </Routes>
      </div>
    </Router>
  );
}

export default App;
