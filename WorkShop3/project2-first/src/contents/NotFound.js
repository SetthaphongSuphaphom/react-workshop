import React, { Component } from "react";
import notfound from "../images/404notfound.png";

class NotFound extends Component {
  render() {
    return (
      <div className=" container">
        <h2 className="title">ไม่พบหน้าเว็บ (404 Page Not Found)</h2>
        <img src={notfound} alt="not found" />
      </div>
    );
  }
}

export default NotFound;
