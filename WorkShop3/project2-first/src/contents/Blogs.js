import React, { Component } from "react";
import blogs from "../data/blogs";
import "./Blogs.css";
import { Link } from "react-router-dom";

class Blogs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      search: "",
      filteredBlogs: blogs,
    };
  }

  handleSearch = (e) => {
    const searchValue = e.target.value.toLowerCase();
    const filteredBlogs = blogs.filter((item) =>
      item.title.toLowerCase().includes(searchValue)
    );
    this.setState({ search: searchValue, filteredBlogs });
  };

  render() {
    const { filteredBlogs } = this.state;
    return (
      <div>
        <div className="container">
          <h2 className="title">All Blogs</h2>
        </div>
        <div className="blogs-container">
          <div className="search-container">
            <input
              type="text"
              className="search-input"
              placeholder="ค้นหาบทความ"
              onChange={this.handleSearch}
            />
          </div>
          <article>
            {filteredBlogs.map((item) => (
              <Link to={`/blog/${item.id}`} key={item.id}>
                <div className="card" key={item.id}>
                  <h2>{item.title}</h2>
                  <p>{item.content.substring(0, 300)}</p>
                  <hr />
                </div>
              </Link>
            ))}
          </article>
        </div>
      </div>
    );
  }
}

export default Blogs;
