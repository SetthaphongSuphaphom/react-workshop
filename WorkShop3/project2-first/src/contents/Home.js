import React, { Component } from "react";
import home from "../images/download.png";

class Home extends Component {
  render() {
    return (
      <div className="container">
        <h2 className="title">Home</h2>
        <img src={home} alt="home" />
      </div>
    );
  }
}

export default Home;
