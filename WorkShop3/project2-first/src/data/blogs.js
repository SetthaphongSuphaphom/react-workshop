const blogs=[
    {
        id:1,
        title:"รู้จักกับส้มตำไทย",
        image_url:"https://cdn.pixabay.com/photo/2015/04/07/07/51/papaya-salad-710613_1280.jpg",
        content:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Massa placerat duis ultricies lacus. Arcu cursus euismod quis viverra nibh cras pulvinar. Aenean et tortor at risus. Lorem donec massa sapien faucibus et molestie ac feugiat. Elit at imperdiet dui accumsan sit amet. Imperdiet dui accumsan sit amet nulla facilisi morbi tempus iaculis. Est placerat in egestas erat imperdiet sed euismod nisi. Ornare arcu dui vivamus arcu felis bibendum ut tristique et. Neque convallis a cras semper auctor. Sapien nec sagittis aliquam malesuada bibendum. Scelerisque mauris pellentesque pulvinar pellentesque habitant. Et malesuada fames ac turpis egestas integer eget aliquet.",
        author:"ก้องรักสยาม"
    },
    {
        id:2,
        title:"ข้าวต้มมัด",
        image_url:"https://cdn.pixabay.com/photo/2021/12/09/23/51/bundled-boiled-rice-6859454_1280.jpg",
        content:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Massa placerat duis ultricies lacus. Arcu cursus euismod quis viverra nibh cras pulvinar. Aenean et tortor at risus. Lorem donec massa sapien faucibus et molestie ac feugiat. Elit at imperdiet dui accumsan sit amet. Imperdiet dui accumsan sit amet nulla facilisi morbi tempus iaculis. Est placerat in egestas erat imperdiet sed euismod nisi. Ornare arcu dui vivamus arcu felis bibendum ut tristique et. Neque convallis a cras semper auctor. Sapien nec sagittis aliquam malesuada bibendum. Scelerisque mauris pellentesque pulvinar pellentesque habitant. Et malesuada fames ac turpis egestas integer eget aliquet.",
        author:"โจโจ้"
    },
    {
        id:3,
        title:"ยำทะเล",
        image_url:"https://cdn.pixabay.com/photo/2019/09/15/08/06/thaifood-4477570_960_720.jpg",
        content:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Massa placerat duis ultricies lacus. Arcu cursus euismod quis viverra nibh cras pulvinar. Aenean et tortor at risus. Lorem donec massa sapien faucibus et molestie ac feugiat. Elit at imperdiet dui accumsan sit amet. Imperdiet dui accumsan sit amet nulla facilisi morbi tempus iaculis. Est placerat in egestas erat imperdiet sed euismod nisi. Ornare arcu dui vivamus arcu felis bibendum ut tristique et. Neque convallis a cras semper auctor. Sapien nec sagittis aliquam malesuada bibendum. Scelerisque mauris pellentesque pulvinar pellentesque habitant. Et malesuada fames ac turpis egestas integer eget aliquet.",
        author:"ก้องรักสยาม"
    },
    {
        id:4,
        title:"พระอาทิตย์ตกดิน",
        image_url:"https://cdn.pixabay.com/photo/2015/03/22/23/07/bora-bora-685303_1280.jpg",
        content:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Massa placerat duis ultricies lacus. Arcu cursus euismod quis viverra nibh cras pulvinar. Aenean et tortor at risus. Lorem donec massa sapien faucibus et molestie ac feugiat. Elit at imperdiet dui accumsan sit amet. Imperdiet dui accumsan sit amet nulla facilisi morbi tempus iaculis. Est placerat in egestas erat imperdiet sed euismod nisi. Ornare arcu dui vivamus arcu felis bibendum ut tristique et. Neque convallis a cras semper auctor. Sapien nec sagittis aliquam malesuada bibendum. Scelerisque mauris pellentesque pulvinar pellentesque habitant. Et malesuada fames ac turpis egestas integer eget aliquet.",
        author:"นักเขียนอิสระ"
    }
]
export default blogs;