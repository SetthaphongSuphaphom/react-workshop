import React from "react";
import "./Item.css";
import { BsFillTrashFill } from "react-icons/bs";
import { BsFillFileEarmarkRichtextFill } from "react-icons/bs";

const Item = ({ data, deleteTasks, editTasks }) => {
  try {
    return (
      <div className="list-item">
        <p className="title">
          {data.id} - {data.title}
        </p>
        <div className="button-container">
          <BsFillTrashFill
            className="btn"
            onClick={() => deleteTasks(data.id)}
          />

          <BsFillFileEarmarkRichtextFill
            className="btn"
            onClick={() => editTasks(data.id)}
          />
        </div>
      </div>
    );
  } catch (error) {
    console.log("error " + error.message);
  }
};

export default Item;
