import React from "react";
import home from "../assets/image/home.png";
import "../components/Home.css";

const Home = () => {
  return (
    <div className="container">
      <div className="title">Home</div>
      <img src={home} alt="home" />
    </div>
  );
};

export default Home;
