import React from "react";
import { Link } from "react-router-dom";
import "../components/Navbar.css";

const Navbar = () => {
  return (
    <nav>
      <Link to="/" className="logo">
        <h3>Blogs Application</h3>
      </Link>
      <Link to="/home" className="nav-item">
        Home
      </Link>
      <Link to="/blogs" className="nav-item">
        Blogs
      </Link>
      <Link to="/about" className="nav-item">
        About
      </Link>
    </nav>
  );
};

export default Navbar;
