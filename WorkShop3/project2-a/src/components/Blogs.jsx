import React, { useState, useEffect } from "react";
import data from "../data/data.jsx";
import "../components/Blogs.css";
import { Link } from "react-router-dom";

const Blogs = () => {
  const [search, setSearch] = useState("");
  const [newData, setNewData] = useState([]);
  useEffect(() => {
    const newSearch = data.filter((data) => data.title.includes(search)); //หรือใช้ toLoewrcase
    console.log("ข้อมูลที่มีการ search ", newSearch);
    setNewData(newSearch);
  }, [search]);
  return (
    <div className="data-container">
      <div className="search-container">
        <input
          type="text"
          className="search-input"
          placeholder="ค้นหาบทความ"
          value={search}
          onChange={(e) => setSearch(e.target.value)}
        />
      </div>
      <article>
        {newData.map((data) => (
          <Link to={`/blog/${data.id}`} key={data.id}>
            <div className="card">
              <h2>{data.title}</h2>
              <p>{data.content.substring(0, 300)}</p>
              <hr />
            </div>
          </Link>
        ))}
      </article>
    </div>
  );
};

export default Blogs;
