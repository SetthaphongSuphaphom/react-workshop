import React from "react";
import about from "../assets/image/about-us-page.png";
const About = () => {
  return (
    <div className="container">
      <div className="title">About Us</div>
      <img src={about} alt="about" />
    </div>
  );
};

export default About;
