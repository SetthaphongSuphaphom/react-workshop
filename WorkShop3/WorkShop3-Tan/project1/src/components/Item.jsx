import { BsTrash} from "react-icons/bs";
import { BiEdit} from "react-icons/bi";
import React from "react";
import "./Item.css";

function Item(props){
  const {data,deleteTask,editTask} = props
  return(
    <div className="list-item">
        <p className="title">  {data.title}</p>
        <div className="button-container">
            
            <BsTrash className="btn" onClick={()=>deleteTask(data.id)}>ลบ</BsTrash>
            <BiEdit className="btn"onClick={()=>editTask(data.id)}>แก้ไข</BiEdit>
        </div>
    </div>
  )
}

export default Item;
