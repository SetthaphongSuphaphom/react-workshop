import { Link } from "react-router-dom";
import './Navbar.css'
export default function Navbar(){
    return(
        <nav>
            <div className="header">
                <span>Blog Application</span>
            </div>
            <ul className="links">
                <li><Link to="/" className="link">Home</Link></li>
                <li><Link to="Blogs" className="link">Blogs</Link></li>
                <li><Link to="About" className="link">About</Link></li>
            </ul>
        </nav>
    )
}