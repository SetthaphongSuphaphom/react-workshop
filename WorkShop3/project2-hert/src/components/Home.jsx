import home from "../assets/Home.jpg";

export default function () {
  return (
    <div className="container">
      <h2 className="title">หน้าแรกของเว็บไซต์</h2>
      <img src={home} alt="Home"></img>
    </div>
  );
}
