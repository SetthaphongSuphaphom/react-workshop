import { Routes, Route } from "react-router-dom";
import './CSS/App.css'
import Home from "./components/Home";
import Report from "./components/Report";
import FormReport from "./components/FormReport";
import Detail from "./components/Detail";

function App() {

  return (
    <div>
      
      <Routes>
        <Route path="/" element={<Detail />} />
        <Route path="Report" element={<Report />} />
        <Route path="FormReport" element={<FormReport />} />
        <Route path="Detail" element={<Detail />} />
      </Routes>
    </div>
  );
}

export default App