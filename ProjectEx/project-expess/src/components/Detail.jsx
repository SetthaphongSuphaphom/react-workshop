import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import ExpandLessIcon from "@mui/icons-material/ExpandLess";
import InputLabel from "@mui/material/InputLabel";
import Button from "@mui/material/Button";
import Checkbox from "@mui/material/Checkbox";
import FormGroup from "@mui/material/FormGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import FilledInput from "@mui/material/FilledInput";
import FormControl from "@mui/material/FormControl";
import NativeSelect from "@mui/material/NativeSelect";
import TextField from "@mui/material/TextField";
import "../CSS/Detail.css";
import Navbar from "./Navbar";
import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos';

export default function Detail() {
  return (
    <body>
      <Navbar detail="รายละเอียดการจัดส่ง" backIcon={<ArrowBackIosIcon/>}/>
      <div>
        <div className="form">
          <Box
            id="head"
            sx={{
              display: "flex",
              justifyContent: "space-between",
              padding: "2%",
            }}
          >
            <Typography id="header-word">จุดเริ่ม</Typography>
            <Button style={{ color: 'black' }}>
              <ExpandLessIcon />
            </Button>
          </Box>
          <Box id="Box-content">
            <TextField
              fullWidth
              id="standard-basic"
              label="ชื่อ - นามสกุล"
              variant="standard"
            />
            <TextField
              fullWidth
              id="standard-basic"
              label="เบอร์โทร"
              variant="standard"
            />
            <TextField
              fullWidth
              id="standard-basic"
              label="รายละเอียดที่อยู่"
              variant="standard"
            />

            <FormControl sx={{ marginTop: "3%" }} fullWidth variant="filled">
              <InputLabel
                htmlFor="filled-adornment-amount"
                sx={{ fontSize: "100%" }}
              >
                รายละเอียดเพิ่มเติม
              </InputLabel>
              <FilledInput
                id="filled-adornment-amount"
                label="Multiline"
                multiline
                rows={3}
                sx={{ backgroundColor: "white" }}
              />
            </FormControl>
          </Box>
        </div>

        <div className="form">
          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              padding: "2%",
            }}
          >
            <Typography id="header-word">จุดที่ 2</Typography>
            <Button style={{ color: 'black' }}> 
              <ExpandLessIcon />
            </Button>
          </Box>
          <Box id="Box-content">
            <TextField
              fullWidth
              id="standard-basic"
              label="ชื่อ - นามสกุล"
              variant="standard"
            />
            <TextField
              fullWidth
              id="standard-basic"
              label="เบอร์โทร"
              variant="standard"
            />
            <TextField
              fullWidth
              id="standard-basic"
              label="รายละเอียดที่อยู่"
              variant="standard"
            />

            <FormControl sx={{ marginTop: "3%" }} fullWidth variant="filled">
              <InputLabel
                htmlFor="filled-adornment-amount"
                sx={{ fontSize: "100%" }}
              >
                รายละเอียดเพิ่มเติม
              </InputLabel>
              <FilledInput
                id="filled-adornment-amount"
                label="Multiline"
                multiline
                rows={3}
                sx={{ backgroundColor: "white" }}
              />
            </FormControl>
          </Box>
        </div>

        <Box id="add-place">
          <Button
            variant="outlined"
            sx={{
              color: "black",
              borderColor: "black",
              border:"1.5px solid",
              width: "100%",
              fontSize: "120%",
              borderRadius: "0.4rem",
              minWidth: "300px",
            }}
          >
            เพิ่มจุด
          </Button>
        </Box>

        <Box id="detail-weight-transportation">
          <TextField
            required
            fullWidth
            id="standard-basic"
            label="น้ำหนักรวม (กิโลกรัม)"
            variant="standard"
          />

          <FormControl fullWidth>
            <InputLabel
              variant="standard"
              htmlFor="uncontrolled-native"
            ></InputLabel>
            <NativeSelect>
              <option>จักรยานยนต์</option>
              <option>รถยนต์</option>
            </NativeSelect>
          </FormControl>
        </Box>

        <Box id="add-extra">
          <Typography sx={{ fontWeight: "bold", fontSize: "140%" }}>
            บริการเสริม
          </Typography>
          <FormGroup sx={{ marginBottom: "0px" }}>
            <Box id="deal-checkbox">
              <FormControlLabel
                control={<Checkbox />}
                label="เดินเอกสารทั่วไป วางบิลเก็บเช็ค"
              />
              30 ฿
            </Box>
            <Box id="deal-checkbox">
              <FormControlLabel
                control={<Checkbox />}
                label="นำเงิน/เช็ค เข้าบัญชี (สาขาที่ใกล้ที่สุด)"
              />
              30 ฿
            </Box>
            <Box id="deal-checkbox">
              <FormControlLabel
                control={<Checkbox />}
                label="เก็บเงินปลายทางนำมาส่ง"
              />
              30 ฿
            </Box>
            <Box id="deal-checkbox">
              <FormControlLabel
                control={<Checkbox />}
                label="ต้องการกล่องใส่ของ/อาหาร"
              />
              30 ฿
            </Box>
            <Box id="deal-checkbox">
              <FormControlLabel
                control={<Checkbox />}
                label="ฝากส่งสินค้าโดยเครื่องบิน"
              />
              30 ฿
            </Box>
            <Box id="deal-checkbox">
              <FormControlLabel control={<Checkbox />} label="ระบุตัวผู้ขับ" />
            </Box>
          </FormGroup>

          <Box sx={{ float: "right", width: "92%", marginTop: "-5%" }}>
            <FormControl fullWidth>
              <InputLabel
                variant="standard"
                htmlFor="uncontrolled-native"
              ></InputLabel>
              <NativeSelect>
                <option>เลือกผู้ขับขี่ที่ต้องการ</option>
                <option>รถยนต์</option>
                <option>จักรยานยนต์</option>
              </NativeSelect>
            </FormControl>
          </Box>
        </Box>

        <Box id="summary-box">
          <Box>
            <Box sx={{ display: "flex", alignItems: "baseline" }}>
              <Typography id="summary-text">ค่าขนส่ง&nbsp;</Typography>
              <Typography sx={{ fontSize: "80%", color: "gray" }}>
                (บาท)
              </Typography>
            </Box>

            <Typography sx={{ fontSize: "250%", fontWeight: "bold" }}>
              538.00
            </Typography>
          </Box>
          <Button
            sx={{
              color: "white",
              backgroundColor: "black",
              borderRadius: "0.5rem",
              right: "10%",
              fontSize: "100%",
              padding: "4% 10% 4% 10%",
              width: "fit-content",
              height: "100%",
            }}
          >
            ดำเนินการต่อ
          </Button>
        </Box>
      </div>
    </body>
  );
}
